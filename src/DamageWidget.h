﻿#pragma once

#include "AutoObject.h"

namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;

    class Response;
    typedef QSharedPointer<Response> ResponseShp;
}

namespace Ui
{
    class DamageWidget;
}

namespace auto_review_workshop
{
    class DamageWidget : public QFrame
    {
        Q_OBJECT

    public:
        explicit DamageWidget(const AutoDamageShp& damageObj);
        ~DamageWidget();

        inline AutoDamageShp getDamageObj() { return _damage; }
        void setDamageObj(const AutoDamageShp& damageObj);

    signals:
        void signalCloseDamage(DamageWidget *widget);
        void signalOpenDamage(DamageWidget *widget);
        void openImagesClicked(const AutoDamageShp&);

    private slots:
        void changeStatus();
        void onButtonImageClicked();

    private:
        void fillAllField();
        void checkButtons();

        void onChangeStatusDamage(network::ResponseShp);

    private:
        Ui::DamageWidget* _ui;

        AutoDamageShp _damage;
        network::RequestsManagerShp _requestsManager;
        QMap<QPushButton*, QUrl> mapButtonPhoto;
        QMap<QUrl, QPushButton*> mapPhotos;
    };

}
