#pragma once


namespace Ui
{
    class ImageFullScreen;
}

namespace auto_review_workshop
{

    class ImageFullScreen : public QWidget
    {
        Q_OBJECT

    public:
        explicit ImageFullScreen(QWidget* parent = nullptr);
        ~ImageFullScreen();

        void setImagesList(const QStringList& imagesList);

    signals:
        void closeImages();

    private slots:
        void on_btnRotationLeft_clicked();
        void on_btnRotationRight_clicked();

        void on_btnLeft_clicked();
        void on_btnRight_clicked();

        void on_btnSaveImage_clicked();

    protected:
        void keyPressEvent(QKeyEvent *event) override;
        bool eventFilter(QObject *obj, QEvent *event) override;

    private:
        void changeImage();
        QPixmap checkImageSize(QPixmap &pix);

    private:
        Ui::ImageFullScreen *_ui;

        QGraphicsPixmapItem *_pixmapItem = nullptr;
        QStringList _imagesList;
        quint8 _pos = 0;
        qint64 _rotation = 0;
    };

}
