#pragma once


namespace auto_review_workshop
{

    class AutoDamage;
    typedef QSharedPointer<AutoDamage> AutoDamageShp;

    class PhotoDownloadManager : public QObject
    {
        Q_OBJECT

    public:
        PhotoDownloadManager(QObject *parent = nullptr);
        ~PhotoDownloadManager();

    public slots:
        void start(const AutoDamageShp &damage);
        void slotError(QNetworkReply::NetworkError err);
        void slotSslErrors(QList<QSslError> err);

    private:
        QString saveFileName(const QUrl &url);

    signals:
        void signalStart(const AutoDamageShp &damage);
        void end(const AutoDamageShp& damage);
        void failed(const AutoDamageShp& damage);
        void signalIsBusy(const AutoDamageShp& damage);

    private slots:
        void startNextDownload();
        void downloadFinished();
        void downloadReadyRead();

    private:
        AutoDamageShp _damage;
        QNetworkAccessManager *_manager;
        QQueue<QUrl> _downloadQueue;
        QNetworkReply *_currentReply = nullptr;
        QFile _output;

        QStringList _listPath;

        bool stateDownloadManager;
        QStringList _commentList;

        QString _filename;

        quint8 _idPhoto;
        QUrl _url;
    };

}
