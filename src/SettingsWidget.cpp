#include "Common.h"

#include "SettingsWidget.h"
#include "ui_SettingsWidget.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"


using namespace auto_review_workshop;

SettingsWidget::SettingsWidget(QWidget* parent)
    : QWidget(parent)
    , _ui(new Ui::SettingsWidget)
    , _requestsManager(network::RequestsManagerShp::create())
{
    _ui->setupUi(this);

    connect(_ui->tableViewDamageTypes, &QTableView::clicked, this, &SettingsWidget::onDamageClicked);
    connect(_ui->editCurrentDamageType, &QLineEdit::textChanged, this, &SettingsWidget::onCurrentDamageChanged);
    connect(_ui->btnEditDamage, &QPushButton::clicked, this, &SettingsWidget::onEditDamageClicked);

    connect(_ui->editInsertDamage, &QLineEdit::textChanged, this, &SettingsWidget::onInsertDamageChanged);
    connect(_ui->btnInsertDamage, &QPushButton::clicked, this, &SettingsWidget::onInsertDamageClicked);

    connect(_ui->editMaintenance, &QLineEdit::textChanged, this, &SettingsWidget::onMaintenanceChanged);
    connect(_ui->btnSaveMaintenanceMax, &QPushButton::clicked, this, &SettingsWidget::onMaintenanceSaveClicked);

    _ui->btnEditDamage->setEnabled(false);
    _ui->btnInsertDamage->setEnabled(false);
    _ui->btnSaveMaintenanceMax->setEnabled(false);

    _ui->editCurrentDamageType->setEnabled(false);
    _ui->editInsertDamage->setEnabled(false);
    _ui->editMaintenance->setEnabled(false);

    getDamages();
    getMaintenanceMax();
    _ui->tableViewDamageTypes->setColumnHidden(0, true);
}

SettingsWidget::~SettingsWidget()
{
    delete _ui;
}

void SettingsWidget::onDamageClicked(const QModelIndex &index)
{
    const auto typeIndex = _model->index(index.row(), 1);
    _ui->editCurrentDamageType->setText(_model->data(typeIndex).toString());
}

void SettingsWidget::onCurrentDamageChanged()
{
    if (_model->data(_model->index(_ui->tableViewDamageTypes->currentIndex().row(), 1)).toString() == _ui->editCurrentDamageType->text())
        _ui->btnEditDamage->setEnabled(false);
    else
        _ui->btnEditDamage->setEnabled(true);
}

void SettingsWidget::onEditDamageClicked()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "ws_change_damage_type";

    requestMap["damage_id"] = _model->data(_model->index(_ui->tableViewDamageTypes->currentIndex().row(), 0)).toULongLong();
    requestMap["damage_type"] = _ui->editCurrentDamageType->text();

    network::RequestShp request(new network::DefaultRequest("ws_change_damage_type", requestMap));
    _requestsManager->execute(request, this, &SettingsWidget::editDamageResponse);

    _ui->btnEditDamage->setEnabled(false);
    _ui->editCurrentDamageType->setEnabled(false);
}

void SettingsWidget::onInsertDamageChanged(const QString& text)
{
    if (text.count() < 2)
        _ui->btnInsertDamage->setEnabled(false);
    else
        _ui->btnInsertDamage->setEnabled(true);
}

void SettingsWidget::onInsertDamageClicked()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "ws_add_damage_type";

    const auto lastIndex = _model->index(_model->rowCount() - 1, 0);
    requestMap["damage_id"] = _model->data(lastIndex).toULongLong() + 1;
    requestMap["damage_type"] = _ui->editInsertDamage->text();

    network::RequestShp request(new network::DefaultRequest("ws_add_damage_type", requestMap));
    _requestsManager->execute(request, this, &SettingsWidget::insertDamageResponse);

    _ui->editInsertDamage->setEnabled(false);
    _ui->btnInsertDamage->setEnabled(false);
}

void SettingsWidget::onMaintenanceChanged(const QString &text)
{
    if (text.toULongLong() != _maintenanceMax)
        _ui->btnSaveMaintenanceMax->setEnabled(true);
    else
        _ui->btnSaveMaintenanceMax->setEnabled(false);
}

void SettingsWidget::onMaintenanceSaveClicked()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "ws_change_maintenance_max";
    requestMap["maintenance"] = _ui->editMaintenance->text().toULongLong();

    network::RequestShp request(new network::DefaultRequest("ws_change_maintenance_max", requestMap));
    _requestsManager->execute(request, this, &SettingsWidget::saveMaintenanceResponse);
}

void SettingsWidget::getDamages()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "ws_get_damage_types";

    network::RequestShp request(new network::DefaultRequest("ws_get_damage_types", requestMap));
    _requestsManager->execute(request, this, &SettingsWidget::onGetDamages);
}

void SettingsWidget::onGetDamages(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();
    const auto& head = responseMap["head"].toMap();
    const auto& body = responseMap["body"].toMap();
    const auto& commandType = head["type"].toString();
    if (commandType.compare("ws_get_damage_types") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const qint64 status = body["status"].toLongLong();
    if (status != 1)
    {
        qDebug() << "Does not get damage types";
    }

    const auto& damages = body["damage_types"].toList();
    _model->setColumnCount(2);
    for (const auto& damageV : damages)
    {
        const auto& damage = damageV.toMap();

        QList<QStandardItem*> items;
        QStandardItem* itemId = new QStandardItem(damage["id"].toString());
        QStandardItem* itemText = new QStandardItem(damage["type"].toString());
        items << itemId << itemText;
        _model->appendRow(items);
    }

    _ui->tableViewDamageTypes->setModel(_model);
    _ui->tableViewDamageTypes->setColumnHidden(0, true);
    _ui->tableViewDamageTypes->setColumnWidth(1, 230);
    _model->setHeaderData(1, Qt::Horizontal, "Повреждение");

    _ui->editCurrentDamageType->setEnabled(true);
    _ui->editInsertDamage->setEnabled(true);
}

void SettingsWidget::getMaintenanceMax()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "ws_get_maintenance_max";

    network::RequestShp request(new network::DefaultRequest("ws_get_maintenance_max", requestMap));
    _requestsManager->execute(request, this, &SettingsWidget::onGetMaintenanceMax);
}

void SettingsWidget::onGetMaintenanceMax(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();
    const auto& head = responseMap["head"].toMap();
    const auto& body = responseMap["body"].toMap();
    const auto& commandType = head["type"].toString();
    if (commandType.compare("ws_get_maintenance_max") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const qint64 status = body["status"].toLongLong();
    if (status != 1)
    {
        qDebug() << "Does not change damage type";
        return;
    }

    _maintenanceMax = body["maintenance"].toULongLong();
    _ui->editMaintenance->setText(QString::number(_maintenanceMax));

    _ui->editMaintenance->setEnabled(true);
}

void SettingsWidget::editDamageResponse(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();
    const auto& head = responseMap["head"].toMap();
    const auto& body = responseMap["body"].toMap();
    const auto& commandType = head["type"].toString();
    if (commandType.compare("ws_change_damage_type") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const qint64 status = body["status"].toLongLong();
    if (status != 1)
    {
        qDebug() << "Does not change damage type";

        _ui->btnEditDamage->setEnabled(true);
        _ui->editCurrentDamageType->setEnabled(true);

        return;
    }

    _ui->btnEditDamage->setEnabled(false);
    _ui->editCurrentDamageType->setEnabled(true);

    const auto index = _model->index(_ui->tableViewDamageTypes->currentIndex().row(), 1);
    _model->setData(index, _ui->editCurrentDamageType->text());
}

void SettingsWidget::insertDamageResponse(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();
    const auto& head = responseMap["head"].toMap();
    const auto& body = responseMap["body"].toMap();
    const auto& commandType = head["type"].toString();
    if (commandType.compare("ws_add_damage_type") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const qint64 status = body["status"].toLongLong();
    if (status != 1)
    {
        qDebug() << "Does not add new damage type";

        _ui->btnInsertDamage->setEnabled(true);
        _ui->editInsertDamage->setEnabled(true);

        return;
    }

    _ui->btnInsertDamage->setEnabled(false);
    _ui->editInsertDamage->setEnabled(true);
    _ui->editInsertDamage->clear();

    QList<QStandardItem*> items;
    QStandardItem* itemId = new QStandardItem(body["damage_id"].toString());
    QStandardItem* itemText = new QStandardItem(body["damage_type"].toString());
    items << itemId << itemText;

    _model->appendRow(items);
}

void SettingsWidget::saveMaintenanceResponse(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();
    const auto& head = responseMap["head"].toMap();
    const auto& body = responseMap["body"].toMap();
    const auto& commandType = head["type"].toString();
    if (commandType.compare("ws_change_maintenance_max") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const qint64 status = body["status"].toLongLong();
    if (status != 1)
    {
        qDebug() << "Does not change maintenance";

        _ui->btnSaveMaintenanceMax->setEnabled(true);
        _ui->editMaintenance->setEnabled(true);

        return;
    }

    _ui->btnSaveMaintenanceMax->setEnabled(false);
    _maintenanceMax = _ui->editMaintenance->text().toULongLong();
}
