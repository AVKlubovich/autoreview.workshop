﻿#pragma once

#include "AutoObject.h"


namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;

    class Response;
    typedef QSharedPointer<Response> ResponseShp;
}

namespace session_controller
{
    class Session;
    typedef QSharedPointer<Session> SessionShp;
}

namespace Ui
{
    class MainWindow;
}

namespace auto_review_workshop
{

    class CheckedModel;
    class AutoRowWidget;

    class MainWindow : public QMainWindow
    {
        Q_OBJECT

    public:
        explicit MainWindow();
        ~MainWindow();

        void setSession(const session_controller::SessionShp &session);

    signals:
        void sCompareAutoObj();
        void sCompareAutoDamageObj();
        void signalFilterChanged(const quint8, const QString);

    private slots:
        void onOpenInfoClicked(AutoObjectShp autoObj);
        void onCloseInfoClicked();
        void onOpenImagesClicked(const AutoDamageShp& damageObj);
        void onCloseImagesClicked();
        void onCloseInfoAndParkClicked();
        void onOpenAdminToolsClicked();
        void onCloseAdminToolsClicked();
        void onFindCarsChanged();
        void onFetchData();

    private:
        void getStartData();
        void onGetStartData(network::ResponseShp response);
        void fetchData(network::ResponseShp response);

        void createAutoObject(const QVariantMap& autoData);
        AutoDamageShp createDamageObject(const QVariantMap& damageData);

        void timerEvent(QTimerEvent *event);
        void createAutoparksModel(const QVariantList &list);
        void createStatusesModel(const QVariantList &list);

        AutoObjectShp compareDataAuto(AutoObjectShp autoObj, const QVariantMap &autoData);
        bool compareDataAutoDamage(AutoDamageShp damageObj, const QVariantMap &autoData);

    private:
        Ui::MainWindow *_ui;

        QTimer _fetchTimer;

        QList<AutoObjectShp> _autos;
        QList<AutoRowWidget*> _listAutoWidgets;

        QVBoxLayout _layout;
        CheckedModel* _modelAutoparks = nullptr;
        CheckedModel* _modelStatuses = nullptr;

        AutoRowWidget *_widgetOpen = nullptr;

        network::RequestsManagerShp _requestsManager;
        session_controller::SessionShp _session;

        const QString WINDOW_TITLE = "AutoReview. Workshop tools.";
    };

}
