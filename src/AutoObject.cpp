#include "Common.h"
#include "AutoObject.h"

#include "utils/BaseClasses/MetaTypeRegistration.h"

RegisterMetaType(QSharedPointer<auto_review_workshop::AutoDamage>)


using namespace auto_review_workshop;

AutoObject::AutoObject()
{
}

AutoObject::~AutoObject()
{
}

bool AutoObject::checkFilter(const QString &filterStr)
{
    if (QString::number(_id).contains(filterStr) ||
        QString::number(_idCar).contains(filterStr) ||
        _insurance.contains(filterStr) ||
        QString::number(_maintenance).contains(filterStr) ||
        _marka.contains(filterStr) ||
        QString::number(_mileage).contains(filterStr) ||
        _model.contains(filterStr) ||
        _number.contains(filterStr) ||
        _color.contains(filterStr))
    {
        return true;
    }

    return false;
}

AutoDamage::AutoDamage()
{
}

AutoDamage::~AutoDamage()
{
}
