#include "Common.h"
#include "ImageFullScreen.h"
#include "ui_ImageFullScreen.h"


using namespace auto_review_workshop;

ImageFullScreen::ImageFullScreen(QWidget *parent)
    : QWidget(parent)
    , _ui(new Ui::ImageFullScreen)
{
    _ui->setupUi(this);

    QGraphicsScene* scene = new QGraphicsScene(this);
    _ui->graphicsView->setScene(scene);
    scene->installEventFilter(this);

    connect(_ui->btnClose, &QPushButton::clicked, this, &ImageFullScreen::closeImages);
}

ImageFullScreen::~ImageFullScreen()
{
    delete _ui;
}

void ImageFullScreen::setImagesList(const QStringList &imagesList)
{
    _imagesList = imagesList;
    _pos = 0;
    _rotation = 0;

    _ui->graphicsView->scene()->setSceneRect(_ui->graphicsView->viewport()->rect());

    changeImage();
}

void ImageFullScreen::on_btnRotationLeft_clicked()
{
    _rotation -= 90;
    changeImage();
}

void ImageFullScreen::on_btnRotationRight_clicked()
{
    _rotation += 90;
    changeImage();
}

void ImageFullScreen::on_btnLeft_clicked()
{
    if (_pos == 0)
        _pos = _imagesList.count();
    --_pos;

    _rotation = 0;
    changeImage();
}

void ImageFullScreen::on_btnRight_clicked()
{
    ++_pos;
    if (_pos == _imagesList.count())
        _pos = 0;

    _rotation = 0;
    changeImage();
}

void ImageFullScreen::on_btnSaveImage_clicked()
{
    const QString& fileName = QFileDialog::getSaveFileName(
                                  this,
                                  tr("Сохранить файл"),
                                  QString("save_photo_%1.%2").arg(QDateTime::currentDateTime().toString("dd_MM_yy_hh_mm_ss")).arg("jpg"),
                                  tr("Изображение (*.jpg, *.jpeg)"));

    if (!fileName.isEmpty())
    {
        bool ok = _pixmapItem->pixmap().save(fileName);
        if (!ok)
        {
            qDebug() << "Image did not save";
        }
    }
}

void ImageFullScreen::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
    {
        emit closeImages();
        event->accept();
    }

    QWidget::keyPressEvent(event);
}

bool ImageFullScreen::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::GraphicsSceneMouseRelease)
    {
        emit closeImages();
        return true;
    }
    return QWidget::eventFilter(obj, event);
}

void ImageFullScreen::changeImage()
{
    _ui->graphicsView->scene()->clear();
    QPixmap pix(_imagesList.at(_pos));

    pix = checkImageSize(pix);

    auto scene = _ui->graphicsView->scene();
    _pixmapItem = scene->addPixmap(pix);

    scene->setSceneRect(_pixmapItem->sceneBoundingRect());
}

QPixmap ImageFullScreen::checkImageSize(QPixmap& pix)
{
    QTransform transform;
    transform.rotate(_rotation);

    pix = pix.transformed(transform);

    const QSize viewSize = _ui->graphicsView->viewport()->size();
    if (pix.width() < viewSize.width() &&
        pix.height() < viewSize.height())
        pix = pix.scaledToWidth(viewSize.width());
    else if (pix.width() > viewSize.width())
        pix = pix.scaledToWidth(viewSize.width());
    if (pix.height() > viewSize.height())
        pix = pix.scaledToHeight(viewSize.height());

    return pix;
}
