﻿#pragma once


namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;

    class Response;
    typedef QSharedPointer<Response> ResponseShp;
}

namespace Ui
{
    class SettingsWidget;
}

namespace auto_review_workshop
{

    class DamageTypesModel : public QStandardItemModel
    {
    public:
        DamageTypesModel(QObject *parent = nullptr)
            : QStandardItemModel(parent)
        {
        }

        QVariant data (const QModelIndex& index, int role = Qt::DisplayRole) const
        {
            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;
            return QStandardItemModel::data(index, role);
        }
    };


    class SettingsWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit SettingsWidget(QWidget* parent = nullptr);
        ~SettingsWidget();

    private slots:
        void onDamageClicked(const QModelIndex &index);
        void onCurrentDamageChanged();
        void onEditDamageClicked();

        void onInsertDamageChanged(const QString &text);
        void onInsertDamageClicked();

        void onMaintenanceChanged(const QString& text);
        void onMaintenanceSaveClicked();

    private:
        void getDamages();
        void onGetDamages(network::ResponseShp response);
        void getMaintenanceMax();
        void onGetMaintenanceMax(network::ResponseShp response);

        void editDamageResponse(network::ResponseShp response);
        void insertDamageResponse(network::ResponseShp response);

        void saveMaintenanceResponse(network::ResponseShp response);

    private:
        Ui::SettingsWidget *_ui;
        network::RequestsManagerShp _requestsManager;

        DamageTypesModel *_model = new DamageTypesModel(this);
        quint64 _maintenanceMax = 0;
    };

}
