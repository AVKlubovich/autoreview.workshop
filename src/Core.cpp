﻿#include "Common.h"
#include "Core.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"

#include "utils/Logging/Logger.h"
#include "utils/Logging/LoggerMessages.h"
#include "utils/Logging/Devices/FileDevice.h"
#include "utils/Logging/Devices/DebuggerDevice.h"
#include "utils/Logging/Devices/ConsoleDevice.h"

#include "session-controller/Session.h"
#include "session-controller/SessionController.h"

#include "MainWindow.h"


using namespace auto_review_workshop;

Core::Core()
{
    QDir::setCurrent(QCoreApplication::applicationDirPath());
}

bool Core::init()
{
    qsrand(QTime(0, 0, 0).msecsTo(QTime::currentTime()));

    readConfig();

    if (!initLogger())
    {
        qDebug() << "Logger does not initialize";
        return false;
    }

    if (!initSession())
    {
        qDebug() << "Session does not initialize";
        return false;
    }

    if (!initWindow())
    {
        qWarning() << "Could not initialize server";
        return false;
    }

    return true;
}

void Core::run()
{
    _window->show();
}

void Core::readConfig()
{
    utils::Settings::Options config = { "configuration/client.ini", true };
    utils::SettingsFactory::instance().registerSettings("client", config);

    auto settings = utils::SettingsFactory::instance().settings("client");
    settings =
    {
        // Client
        { "Client/FetchData", 5000 },
        { "Client/ImagesFolder", "AutoReviewData" },

        // Connection
        { "Connection/Scheme", "http" },
        { "Connection/Host", "localhost" },
        { "Connection/Port", "81" },

        // Logs
        { "Log/EnableLog", true },
        { "Log/FlushInterval", 1000 },
        { "Log/PrefixName", "test_client.log" },
        { "Log/Dir", "./logs/" },
        { "Log/MaxSize", 134217728 }, // 100 mb
    };

    utils::SettingsFactory::instance().setCurrentSettings("client");

#ifdef QT_DEBUG
    utils::Settings::Options config222 = { "configuration/_client.ini", true };
    utils::SettingsFactory::instance().registerSettings("client222", config222);

//    utils::SettingsFactory::instance().setCurrentSettings("client222");
#endif
}

bool Core::initLogger()
{
    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Log");

    if (!settings["EnableLog"].toBool())
    {
        qDebug() << "logger disabled";
        return true;
    }

    _logger = QSharedPointer<utils::Logger>::create();

    const auto loggerOptions = QSharedPointer<utils::LoggerMessages::Options>::create();
    loggerOptions->timerInterval = settings["FlushInterval"].toInt();
    if (!_logger->init(loggerOptions))
        return false;

    // FileDevice
    const auto fileOptions = QSharedPointer<utils::FileDevice::FileOptions>::create();
    fileOptions->maxSize = settings["MaxSize"].toLongLong();
    fileOptions->prefixName = settings["PrefixName"].toString(); //"okk_server.log";
    fileOptions->directory = settings["Dir"].toString();

    if (!_logger->addDevice(fileOptions))
        return false;

    // DebuggerDevice
    const auto debuggerDevice = QSharedPointer<utils::DebuggerDevice::DebuggerOptions>::create();

    if (!_logger->addDevice(debuggerDevice))
        return false;

    // ConsoleDevice
    const auto consoleDevice = QSharedPointer<utils::ConsoleDevice::ConsoleOptions>::create();

    if (!_logger->addDevice(consoleDevice))
        return false;

    qDebug() << "initLogger";
    return true;
}


bool Core::initSession()
{
    _session = session_controller::SessionShp::create();
    auto sessionController = session_controller::SessionControllerShp::create(_session);

#ifdef QT_DEBUG
    QString login("test_andrej");
    QString password("andrttest");
    sessionController->setDefaultCredentials(login, password);
#endif

    return sessionController->requestUserCredentials();
}

bool Core::initWindow()
{
    _window = auto_review_workshop::MainWindowShp::create();
    _window->setSession(_session);
    return true;
}

void Core::done()
{
    _window.clear();
    _logger.clear();
}
