#include "Common.h"
#include "PhotoDownloadManager.h"

#include "AutoObject.h"

#include "utils/Settings/SettingsFactory.h"
#include "utils/Settings/Settings.h"


using namespace auto_review_workshop;

PhotoDownloadManager::PhotoDownloadManager(QObject* parent)
    : QObject(parent)
    , _manager(new QNetworkAccessManager(this))
{
    connect(this, &PhotoDownloadManager::signalStart, this, &PhotoDownloadManager::start, Qt::QueuedConnection);
}

PhotoDownloadManager::~PhotoDownloadManager()
{
}

void PhotoDownloadManager::start(const AutoDamageShp &damage)
{
    if (QThread::currentThread() != thread())
    {
        emit signalStart(damage);
        return;
    }

    _damage = damage;
    stateDownloadManager = true;
    _listPath.clear();

    if (_damage->_photos.isEmpty())
        return;

    for (const auto& url : _damage->_photos)
        _downloadQueue.enqueue(url);
    startNextDownload();
}

void PhotoDownloadManager::slotError(QNetworkReply::NetworkError err)
{
    qDebug() << __FUNCTION__ << err;
    qDebug() << _url.toString();
    startNextDownload();
}

void PhotoDownloadManager::slotSslErrors(QList<QSslError> err)
{
    qDebug() << __FUNCTION__ << err;
}

QString PhotoDownloadManager::saveFileName(const QUrl &url)
{
    const auto path = url.path();

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Client");
    const auto& imagesFolder = settings["ImagesFolder"].toString();

    QDir imagesDir(QDir::home());
    imagesDir.cd("Documents");
    if (!imagesDir.exists(imagesFolder))
    {
        const auto mkpathResult = imagesDir.mkpath(imagesFolder);
        if (!mkpathResult)
        {
            Q_ASSERT_X(mkpathResult, __FUNCTION__, "imagesDir.mkpath == false");
            return QString();
        }
    }
    imagesDir.cd(imagesFolder);

    QString basename = QString("%1/%2").arg(imagesDir.absolutePath()).arg(QFileInfo(path).fileName());

    if (basename.isEmpty())
        basename = "download";

    return basename;
}

void PhotoDownloadManager::startNextDownload()
{
    if (_downloadQueue.isEmpty())
    {
        for (const auto& path : _listPath)
            _damage->_downloadedPhotos << path;

        _listPath.clear();

        emit end(_damage);

        return;
    }

    _url = _downloadQueue.dequeue();

    if (!_output.exists() && !_url.isEmpty())
    {
        QNetworkRequest request(_url);
        _currentReply = _manager->get(request);

        connect(_currentReply, &QNetworkReply::finished,
                this, &PhotoDownloadManager::downloadFinished);

        connect(_currentReply, &QNetworkReply::readyRead,
                this, &PhotoDownloadManager::downloadReadyRead);

        connect(_currentReply, static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
                this, &PhotoDownloadManager::slotError);

        connect(_currentReply, &QNetworkReply::sslErrors,
                this, &PhotoDownloadManager::slotSslErrors);
    }
    else
    {
        _listPath << _filename;
        startNextDownload();
    }
}

void PhotoDownloadManager::downloadFinished()
{
    if (_currentReply->error())
    {
        // download failed
        emit failed(_damage);
        qDebug() << "Failed: " + _currentReply->errorString() + "\n";
        _currentReply->deleteLater();
        _currentReply = nullptr;
        _downloadQueue.clear();
        return;
    }
    else
    {
        _listPath << /*_output.fileName();*/ _filename;
    }

    _currentReply->deleteLater();
    _currentReply = nullptr;
    startNextDownload();
}

void PhotoDownloadManager::downloadReadyRead()
{
    if (!_currentReply)
    {
        qDebug() << __FUNCTION__ << "current reply is null";
    }

    QString name = saveFileName(_url);
    QFile file(name);
    _filename = file.fileName();
    if (file.open(QIODevice::Append))
    {
        QByteArray data = _currentReply->readAll();
        file.write(data);
    }
    else
    {
        qDebug() << "file do not open";
    }
    file.close();
}
