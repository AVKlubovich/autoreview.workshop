#include "Common.h"

#include "DamageWidget.h"
#include "ui_DamageWidget.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"

#include "ImageFullScreen.h"


using namespace auto_review_workshop;

DamageWidget::DamageWidget(const AutoDamageShp& damageObj)
    : QFrame(nullptr)
    , _ui(new Ui::DamageWidget)
    , _damage(damageObj)
    , _requestsManager(network::RequestsManagerShp::create())
{
    _ui->setupUi(this);

    fillAllField();

    setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    setLineWidth(2);

    connect(_ui->btnRepair, &QPushButton::clicked, this, &DamageWidget::changeStatus);
    connect(_ui->btnViewImages, &QPushButton::clicked, this, &DamageWidget::onButtonImageClicked);
}

DamageWidget::~DamageWidget()
{
    delete _ui;
}

void DamageWidget::setDamageObj(const AutoDamageShp &damageObj)
{
    _damage = damageObj;

    _ui->lineEditCreate->setText(_damage->_dateCreate);
    _ui->lineEditClose->setText(_damage->_dateClose);

    _ui->labelPhotosCount->setText(QString("%1%2").arg(_ui->labelPhotosCount->text()).arg(_damage->_downloadedPhotos.count()));
}

void DamageWidget::changeStatus()
{
    _damage->_status = !_damage->_status;

    QVariantMap requestMap;
    requestMap["type_command"] = "ws_change_of_status_damages";
    requestMap["id"] = _damage->_id;
    requestMap["status"] = _damage->_status;

    network::RequestShp request(new network::DefaultRequest("ws_change_of_status_damages", requestMap));
    _requestsManager->execute(request, this, &DamageWidget::onChangeStatusDamage);
}

void DamageWidget::onButtonImageClicked()
{
    emit openImagesClicked(_damage);
}

void DamageWidget::fillAllField()
{
    _ui->labelElement->setText(_damage->_elementName);
    _ui->labelDamage->setText(_damage->_type);
    _ui->textBrowserComment->setText(_damage->_comment);

    _ui->lineEditCreate->setText(_damage->_dateCreate);
    _ui->lineEditClose->setText(_damage->_dateClose);

    _ui->labelPhotosCount->setText(QString("%1%2").arg(_ui->labelPhotosCount->text()).arg(_damage->_downloadedPhotos.count()));

    checkButtons();
}

void DamageWidget::checkButtons()
{
    _ui->btnRepair->setChecked(_damage->_status);

    if (_damage->_status)
        emit signalCloseDamage(this);
    else
        emit signalOpenDamage(this);
}

void DamageWidget::onChangeStatusDamage(network::ResponseShp)
{
    checkButtons();
}
