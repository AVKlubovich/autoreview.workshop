#include "Common.h"

#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "utils/Settings/Settings.h"
#include "utils/Settings/SettingsFactory.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"

#include "session-controller/Session.h"
#include "session-controller/SessionController.h"

#include "AutoRowWidget.h"
#include "AutoInfoWidget.h"

#include "CheckedModel.h"

#include "Definitions.h"


using namespace auto_review_workshop;

MainWindow::MainWindow()
    : QMainWindow(nullptr)
    , _ui(new Ui::MainWindow)
    , _requestsManager(network::RequestsManagerShp::create())
{
    _ui->setupUi(this);

    _ui->scrollAreaWidgetContents->setLayout(&_layout);
    startTimer(100);

    setWindowTitle(WINDOW_TITLE);

    _ui->cbPark->setMinimumWidth(200);
    _ui->cbStatus->setMinimumWidth(200);

    _ui->stackedWidget->setCurrentIndex(MAIN_WINDOW_INDEX);

    connect(_ui->infoWidget, &AutoInfoWidget::closeInfoClicked,
            this, &MainWindow::onCloseInfoClicked);
    connect(_ui->infoWidget, &AutoInfoWidget::onOpenImages,
            this, &MainWindow::onOpenImagesClicked);
    connect(_ui->infoWidget, &AutoInfoWidget::closeInfoAndParkClicked,
            this, &MainWindow::onCloseInfoAndParkClicked);

    connect(_ui->imageFullScreenWidget, &ImageFullScreen::closeImages,
            this, &MainWindow::onCloseImagesClicked);

    connect(_ui->btnAdminToolsOpen, &QPushButton::clicked, this, &MainWindow::onOpenAdminToolsClicked);
    connect(_ui->btnAdminBack, &QPushButton::clicked, this, &MainWindow::onCloseAdminToolsClicked);
    connect(_ui->editFind, &QLineEdit::textChanged, this, &MainWindow::onFindCarsChanged);
    connect(_ui->cbStatus, &CheckableComboBox::sClickedCheckedComboBox,
            this, &MainWindow::onFindCarsChanged);
    connect(_ui->cbPark, &CheckableComboBox::sClickedCheckedComboBox,
            this, &MainWindow::onFindCarsChanged);

    setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);

    _modelAutoparks = new CheckedModel(this);
    _ui->cbPark->setModel(_modelAutoparks);

    _modelStatuses = new CheckedModel(this);
    _ui->cbStatus->setModel(_modelStatuses);
}

MainWindow::~MainWindow()
{
    delete _ui;
}

void MainWindow::setSession(const session_controller::SessionShp &session)
{
    _session = session;
    getStartData();
}

void MainWindow::onOpenInfoClicked(AutoObjectShp autoObj)
{
    _ui->stackedWidget->setCurrentIndex(INFO_WIDGET_INDEX);
    _widgetOpen = qobject_cast<AutoRowWidget*>(sender());
    _ui->infoWidget->setAutoObject(autoObj);
}

void MainWindow::onCloseInfoClicked()
{
    _ui->stackedWidget->setCurrentIndex(MAIN_WINDOW_INDEX);
}

void MainWindow::onOpenImagesClicked(const AutoDamageShp &damageObj)
{
    _ui->stackedWidget->setCurrentIndex(IMAGES_FULL_SCREEN_INDEX);
    _ui->imageFullScreenWidget->setImagesList(damageObj->_downloadedPhotos);
}

void MainWindow::onCloseImagesClicked()
{
    _ui->stackedWidget->setCurrentIndex(INFO_WIDGET_INDEX);
}

void MainWindow::onCloseInfoAndParkClicked()
{
    _widgetOpen->onToPark();
    _ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::onOpenAdminToolsClicked()
{
    _ui->stackedWidget->setCurrentWidget(_ui->pageAdminTools);
}

void MainWindow::onCloseAdminToolsClicked()
{
    _ui->stackedWidget->setCurrentWidget(_ui->pageAutoList);
}

void MainWindow::onFindCarsChanged()
{
    QList<quint64> listParks = qobject_cast<CheckedModel *>(_ui->cbPark->model())->checkedRealIndex().toList();
    QList<quint64> listStatuses = qobject_cast<CheckedModel *>(_ui->cbStatus->model())->checkedRealIndex().toList();

    for (auto widget : _listAutoWidgets)
    {
        // find statuses
        if (!listStatuses.contains(widget->getAutoObj()->_status))
        {
            widget->hide();
            continue;
        }

        // find parks
        if (!listParks.contains(widget->getAutoObj()->_idPark))
        {
            widget->hide();
            continue;
        }

        // find text
        if (!widget->matchByFilter(_ui->editFind->text()))
        {
            widget->hide();
            continue;
        }

        widget->show();
    }
}

void MainWindow::onFetchData()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "ws_retrieving_updated_data";
    requestMap["DateTime"] = QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss");

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Client");
    requestMap["FetchData"] = settings["FetchData"].toInt();
    requestMap["login"] = _session->login();
    requestMap["password"] = _session->password();

    network::RequestShp request(new network::DefaultRequest("ws_retrieving_updated_data", requestMap));
    _requestsManager->execute(request, this, &MainWindow::fetchData);
}

void MainWindow::getStartData()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "ws_get_start_data";
    requestMap["login"] = _session->login();
    requestMap["password"] = _session->password();

    network::RequestShp request(new network::DefaultRequest("ws_get_start_data", requestMap));
    _requestsManager->execute(request, this, &MainWindow::onGetStartData);
}

void MainWindow::onGetStartData(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();
    const auto& head = responseMap["head"].toMap();
    const auto& commandType = head["type"].toString();
    if (commandType.compare("ws_get_start_data") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const auto& body = responseMap["body"].toMap();
    const auto& autosList = body["cars"].toList();
    for (const auto& autoData : autosList)
    {
        createAutoObject(autoData.toMap());
    }

    createAutoparksModel(body["autoparks"].toList());
    createStatusesModel(body["statuses"].toList());

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("Client");
    connect(&_fetchTimer, &QTimer::timeout, this, &MainWindow::onFetchData);
    const int fetchData = settings["FetchData"].toInt();
    _fetchTimer.start(fetchData);
}

void MainWindow::fetchData(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();
    const auto& head = responseMap["head"].toMap();
    const auto& commandType = head["type"].toString();
    const auto& body = responseMap["body"].toMap();

    if (commandType.compare("ws_retrieving_updated_data") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const auto& autosList = body["cars"].toList();
    const QVariantMap& damagesMap = body["damageCars"].toMap();

    for (auto autoWidget : _listAutoWidgets)
    {
        const AutoObjectShp &autoObj = autoWidget->getAutoObj();
        QSet<AutoDamageShp> &damagesCar = autoObj->_damages;
        const auto idCarStr = QString::number(autoObj->_idCar);

        if (damagesMap.contains(idCarStr))
        {
            bool flagCreateDamageObj = true;
            const QVariantList &damages = damagesMap.value(idCarStr).toList();

            for (auto damageIt : damages)
            {
                const auto damageId = damageIt.toMap().value("id").toULongLong();
                for (AutoDamageShp damage : damagesCar)
                {
                    if (damage->_id == damageId)
                    {
                        if (compareDataAutoDamage(damage, damageIt.toMap()))
                            _ui->infoWidget->updateDamageWidget(damage);

                        flagCreateDamageObj = false;
                        break;
                    }
                }

                if (flagCreateDamageObj)
                {
                    const AutoDamageShp& damage = createDamageObject(damageIt.toMap());
                    damagesCar << damage;
                    _ui->infoWidget->createDamages();
                }
            }
        }
    }

    for (const auto& autoData : autosList)
    {
        bool flagCreateObj = true;
        const auto idCar = autoData.toMap()["id_car"].toULongLong();

        for (auto autoWidget : _listAutoWidgets)
        {
            if (autoWidget->getIdCar() == idCar)
            {
                auto autoObj = compareDataAuto(autoWidget->getAutoObj(), autoData.toMap());
                _ui->infoWidget->setAutoObject(autoObj, false);
                autoWidget->setAutoObj(autoObj);
                flagCreateObj = false;
            }
        }

        if (flagCreateObj)
            createAutoObject(autoData.toMap());
    }
}

void MainWindow::createAutoObject(const QVariantMap& autoData)
{
    AutoObjectShp autoObj = AutoObjectShp::create();
    autoObj->_id = autoData["id"].toLongLong();
    autoObj->_idCar = autoData["id_car"].toLongLong();
    autoObj->_idPark = autoData["id_park"].toInt();
    autoObj->_marka = autoData["marka"].toString();
    autoObj->_model = autoData["model"].toString();
    autoObj->_color = autoData["color"].toString();
    autoObj->_number = autoData["number"].toString();
    autoObj->_diagnosticCardEnd = autoData["diagnostic_card_end"].toString();
    autoObj->_diagnosticCardData = autoData["diagnostic_card_data"].toString();
    autoObj->_insurance = autoData["insurance_end"].toString();
    autoObj->_mileage = autoData["mileage"].toULongLong();
    autoObj->_status = autoData["status"].toInt();
    autoObj->_maintenance = autoData["last_maintenance"].toUInt();

    for (const auto& historyVal : autoData["history"].toList())
        autoObj->_history << historyVal.toString();

    QSet<AutoDamageShp> damages;
    for (const auto& damageData : autoData["damages"].toList())
    {
        const AutoDamageShp& damage = createDamageObject(damageData.toMap());
        if (!damage)
        {
            // TODO: error
        }

        damages << damage;
    }

    autoObj->_damages = damages;

    AutoRowWidget *newWidget = new AutoRowWidget(autoObj);
    newWidget->setSession(_session);
    connect(newWidget, &AutoRowWidget::openInfoClicked, this, &MainWindow::onOpenInfoClicked);
    connect(this, &MainWindow::signalFilterChanged, newWidget, &AutoRowWidget::setFilterAndCheckVisible);

    _layout.addWidget(newWidget);
    _listAutoWidgets.append(newWidget);
}

AutoDamageShp MainWindow::createDamageObject(const QVariantMap& damageData)
{
    AutoDamageShp damageObj = AutoDamageShp::create();
    damageObj->_id = damageData["id"].toULongLong();
    damageObj->_elementName = damageData["element_name"].toString();
    damageObj->_type = damageData["type"].toString();
    damageObj->_status = damageData["status"].toBool();
    damageObj->_comment = damageData["comment"].toString();
    damageObj->_dateCreate = damageData["date_create"].toDateTime().toString("dd.MM.yyyy HH:mm:ss");
    damageObj->_dateClose = damageData["date_close"].toDateTime().toString("dd.MM.yyyy HH:mm:ss");

    QList<QUrl> photoUrls;
    const QVariantList& photos = damageData["photos"].toList();
    for (const auto& photo : photos)
    {
        const auto& photoMap = photo.toMap();
        photoUrls << photoMap["url"].toUrl();
    }
    damageObj->_photos = photoUrls;

    return damageObj;
}

void MainWindow::timerEvent(QTimerEvent* event)
{
    // NOTE: костыль
    _ui->scrollAreaWidgetContents->adjustSize();
}

void MainWindow::createAutoparksModel(const QVariantList &list)
{
    for (auto it : list)
    {
        auto map = it.toMap();

        QList <QStandardItem*> items;
        items.append(new QStandardItem(map.value("name").toString()));
        items.append(new QStandardItem(map.value("id").toString()));

        _modelAutoparks->appendRow(items);
    }

    _modelAutoparks->checkedAll();
    _ui->cbPark->updateTextHints();
}

void MainWindow::createStatusesModel(const QVariantList &list)
{
    for (auto it : list)
    {
        auto map = it.toMap();

        QList <QStandardItem*> items;
        items.append(new QStandardItem(map.value("name").toString()));
        items.append(new QStandardItem(map.value("id").toString()));

        _modelStatuses->appendRow(items);
    }

    _modelStatuses->checkedAll();
    _ui->cbStatus->updateTextHints();
}

AutoObjectShp MainWindow::compareDataAuto(AutoObjectShp autoObj, const QVariantMap &autoData)
{
    if (autoData["id_car"].toULongLong() != autoObj->_id)
        autoObj->_id = autoData["id_car"].toLongLong();

    if (autoData["id_park"].toULongLong() != autoObj->_idPark)
        autoObj->_idPark = autoData["id_park"].toInt();

    if (autoData["marka"].toString() != autoObj->_marka)
        autoObj->_marka = autoData["marka"].toString();

    if (autoData["model"].toString() != autoObj->_model)
        autoObj->_model = autoData["model"].toString();

    if (autoData["color"].toString() != autoObj->_color)
        autoObj->_color = autoData["color"].toString();

    if (autoData["number"].toString() != autoObj->_number)
        autoObj->_number = autoData["number"].toString();

    if (autoData["diagnostic_card_end"].toString() != autoObj->_diagnosticCardEnd)
        autoObj->_diagnosticCardEnd = autoData["diagnostic_card_end"].toString();

    if (autoData["diagnostic_card_data"].toString() != autoObj->_diagnosticCardData)
        autoObj->_diagnosticCardData = autoData["diagnostic_card_data"].toString();

    if (autoData["insurance_end"].toString() != autoObj->_insurance)
        autoObj->_insurance = autoData["insurance_end"].toString();

    if (autoData["mileage"].toULongLong() != autoObj->_mileage)
        autoObj->_mileage = autoData["mileage"].toULongLong();

    if (autoData["status"].toUInt() != autoObj->_status)
        autoObj->_status = autoData["status"].toInt();

    if (autoData["last_maintenance"].toUInt() != autoObj->_maintenance)
        autoObj->_maintenance = autoData["last_maintenance"].toUInt();

    return autoObj;
}

bool MainWindow::compareDataAutoDamage(AutoDamageShp damageObj, const QVariantMap &damageData)
{
    bool state = false;

    if (damageData["element_name"].toString() != damageObj->_elementName) {
        damageObj->_elementName = damageData["element_name"].toString();
        state = true;
    }

    if (damageData["type"].toString() != damageObj->_type) {
        damageObj->_type = damageData["type"].toString();
        state = true;
    }

    if (damageData["status"].toBool() != damageObj->_status) {
        damageObj->_status = damageData["status"].toBool();
        state = true;
    }

    if (damageData["comment"].toString() != damageObj->_comment) {
        damageObj->_comment = damageData["comment"].toString();
        state = true;
    }

    if (damageData["date_create"].toDateTime().toString("dd.MM.yyyy HH:mm:ss") != damageObj->_dateCreate) {
        damageObj->_dateCreate = damageData["date_create"].toDateTime().toString("dd.MM.yyyy HH:mm:ss");
        state = true;
    }

    if (damageData["date_close"].toDateTime().toString("dd.MM.yyyy HH:mm:ss") != damageObj->_dateClose) {
        damageObj->_dateClose = damageData["date_close"].toDateTime().toString("dd.MM.yyyy HH:mm:ss");
        state = true;
    }

    QList<QUrl> photoUrls = damageObj->_photos;
    const QVariantList& photos = damageData["photos"].toList();

    if (photos.count() != photoUrls.count())
    {
        state = true;
        for (auto it : photos)
        {
            const QVariantMap &mapPhotos = it.toMap();
            QUrl url(mapPhotos.value("url").toString());
            if (!photoUrls.contains(url))
                damageObj->_photos.append(url);
        }
    }

    return state;
}
