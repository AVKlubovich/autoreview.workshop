﻿#pragma once

#include "AutoObject.h"

namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;

    class Response;
    typedef QSharedPointer<Response> ResponseShp;
}

namespace Ui
{
    class AutoInfoWidget;
}

namespace auto_review_workshop
{

    class DamageWidget;

    class AutoInfoWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit AutoInfoWidget(QWidget* parent = nullptr);
        ~AutoInfoWidget();

        void setAutoObject(const AutoObjectShp& autoObj, bool createDamagesFlag = true);
        void createDamages();
        void fillAllField();
        void updateDamageWidget(const AutoDamageShp &damage);

    signals:
        void closeInfoClicked();
        void closeInfoAndParkClicked();
        void onOpenImages(const AutoDamageShp&);
        void onCloseImages();

    private slots:
        void createDamageWidget(const AutoDamageShp& damage);
        void downloadPhotoFailed(const AutoDamageShp& damage);

    private:
        void onDateEditInsurance(network::ResponseShp);
        void onDateEditDiagnosticCard(network::ResponseShp);
        void createConnect();
        void checkMileageButtonsVisible();
        void onChangeMileage(network::ResponseShp);

        void clearLayout();

    private slots:
        void on_dateEditInsurance(const QDate &date);
        void on_dateEditDiagnosticCard(const QDate &date);
        void transferWidget(DamageWidget *damageWidget);
        void transferWidgetClosed(DamageWidget *damageWidget);

        void onMileageChanged();
        void onBtnSaveMileageClicked();
        void onBtnDiscardMileageClicked();

    private:
        Ui::AutoInfoWidget *_ui;

        AutoObjectShp _auto;

        QVBoxLayout* _damageLayout;
        QVBoxLayout* _damageClosedLayout;
        QSet<DamageWidget*> _damageWidgets;
        network::RequestsManagerShp _requestsManager;
    };

}
