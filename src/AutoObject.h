#pragma once


namespace auto_review_workshop
{

    class AutoDamage
    {
    public:
        AutoDamage();
        ~AutoDamage();

    public:
        quint64 _id = 0;
        QString _elementName;
        QString _type;
        QString _comment;
        bool _status;

        QList<QUrl> _photos;
        QList<QString> _downloadedPhotos;

        QString _dateCreate;
        QString _dateClose;
    };

    typedef QSharedPointer<AutoDamage> AutoDamageShp;


    class AutoObject
    {
    public:
        AutoObject();
        ~AutoObject();

        bool checkFilter(const QString& filterStr);

    public:
        quint64 _id = 0;
        quint64 _idCar = 0;
        QString _model;
        QString _marka;
        QString _number;
        QString _color;
        QString _namePark;

        quint64 _idPark = 0;
        quint64 _mileage;
        QString _insurance;
        QString _diagnosticCardEnd;
        QString _diagnosticCardData;
        quint64 _maintenance;

        QSet<AutoDamageShp> _damages;

        QStringList _history;

        QString _dateCreate = "01.01.2017";

        quint8 _status = 1;
    };

    typedef QSharedPointer<AutoObject> AutoObjectShp;

}

Q_DECLARE_METATYPE(auto_review_workshop::AutoObjectShp);
Q_DECLARE_METATYPE(auto_review_workshop::AutoDamageShp);
