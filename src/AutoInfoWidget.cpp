#include "Common.h"

#include "AutoInfoWidget.h"
#include "ui_AutoInfoWidget.h"

#include "DamageWidget.h"
#include "PhotoDownloadManager.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"


using namespace auto_review_workshop;

AutoInfoWidget::AutoInfoWidget(QWidget* parent)
    : QWidget(parent)
    , _ui(new Ui::AutoInfoWidget)
    , _requestsManager(network::RequestsManagerShp::create())
{
    _ui->setupUi(this);

    connect(_ui->btnClose, &QPushButton::clicked,
            [this]()
    {
        disconnect(_ui->dateEditInsurance, &QDateEdit::dateChanged, this, &AutoInfoWidget::on_dateEditInsurance);
        disconnect(_ui->dateEditDiagnosticCard, &QDateEdit::dateChanged, this, &AutoInfoWidget::on_dateEditDiagnosticCard);
        emit closeInfoClicked();
    }
    );

    connect(_ui->btnCloseAndToPark, &QPushButton::clicked,
            [this]()
    {
        disconnect(_ui->dateEditInsurance, &QDateEdit::dateChanged, this, &AutoInfoWidget::on_dateEditInsurance);
        disconnect(_ui->dateEditDiagnosticCard, &QDateEdit::dateChanged, this, &AutoInfoWidget::on_dateEditDiagnosticCard);
        emit closeInfoAndParkClicked();
    }
    );

    _damageLayout = new QVBoxLayout();
    _damageClosedLayout = new QVBoxLayout();
    _ui->frameDamages->setLayout(_damageLayout);

    QRegExp mileageExp("[0-9]{,6}");
    QRegExpValidator *mileageValidator = new QRegExpValidator(mileageExp, _ui->lineEditMileage);
    _ui->lineEditMileage->setValidator(mileageValidator);

    connect(_ui->lineEditMileage, &QLineEdit::textChanged, this, &AutoInfoWidget::onMileageChanged);
    connect(_ui->btnSaveMileage, &QPushButton::clicked, this, &AutoInfoWidget::onBtnSaveMileageClicked);
    connect(_ui->btnDiscardMileage, &QPushButton::clicked, this, &AutoInfoWidget::onBtnDiscardMileageClicked);

    _ui->frameDamagesClosed->setLayout(_damageClosedLayout);
}

AutoInfoWidget::~AutoInfoWidget()
{
    delete _ui;
    delete _damageLayout;
    delete _damageClosedLayout;
}

void AutoInfoWidget::setAutoObject(const AutoObjectShp& autoObj, bool createDamagesFlag)
{
    Q_UNUSED(createDamagesFlag)

    _auto = autoObj;
    fillAllField();
    createDamages();

    checkMileageButtonsVisible();
}

void AutoInfoWidget::createDamages()
{
    clearLayout();

    for (const auto& damage : _auto->_damages)
    {
        PhotoDownloadManager* downloader = new PhotoDownloadManager();
        QThread* downloadThread = new QThread(downloader);
        connect(downloadThread, &QThread::finished, downloader, &PhotoDownloadManager::deleteLater, Qt::QueuedConnection);
        connect(downloader, &PhotoDownloadManager::end, this, &AutoInfoWidget::createDamageWidget, Qt::QueuedConnection);
        connect(downloader, &PhotoDownloadManager::failed, this, &AutoInfoWidget::downloadPhotoFailed, Qt::QueuedConnection);
        downloader->moveToThread(downloadThread);
        downloadThread->start();
        downloader->start(damage);
    }
}

void AutoInfoWidget::fillAllField()
{
    _ui->lineEditMake->setText(_auto->_marka);
    _ui->lineEditModel->setText(_auto->_model);
    _ui->lineEditMileage->setText(QString::number(_auto->_mileage));
    _ui->lineEditNumber->setText(_auto->_number);
    _ui->lineEditColor->setText(_auto->_color);
    _ui->dateEditInsurance->setDate(QDate::fromString(_auto->_insurance, "yyyy-MM-dd"));
    connect(_ui->dateEditInsurance, &QDateEdit::dateChanged, this, &AutoInfoWidget::on_dateEditInsurance);

    _ui->dateEditDiagnosticCard->setDate(QDate::fromString(_auto->_diagnosticCardEnd, "yyyy-MM-dd"));
    connect(_ui->dateEditDiagnosticCard, &QDateEdit::dateChanged, this, &AutoInfoWidget::on_dateEditDiagnosticCard);

    _ui->plainTextEdit->setPlainText(_auto->_history.join("\n"));
}

void AutoInfoWidget::updateDamageWidget(const AutoDamageShp &damage)
{
    for (auto widget : _damageWidgets)
    {
        if (widget->getDamageObj()->_id == damage->_id) {
            widget->setDamageObj(damage);
            break;
        }
    }
}

void AutoInfoWidget::createDamageWidget(const AutoDamageShp &damage)
{
    DamageWidget* newDamageWidget = new DamageWidget(damage);
    connect(newDamageWidget, &DamageWidget::signalCloseDamage, this, &AutoInfoWidget::transferWidgetClosed);
    connect(newDamageWidget, &DamageWidget::signalOpenDamage, this, &AutoInfoWidget::transferWidget);
    connect(newDamageWidget, &DamageWidget::openImagesClicked, this, &AutoInfoWidget::onOpenImages);

    if (damage->_status)
        _damageClosedLayout->addWidget(newDamageWidget);
    else
        _damageLayout->addWidget(newDamageWidget);

    _damageWidgets << newDamageWidget;
}

void AutoInfoWidget::downloadPhotoFailed(const AutoDamageShp &damage)
{
    qDebug() << __FUNCTION__ << "download failed";
}

void AutoInfoWidget::onDateEditInsurance(network::ResponseShp)
{
}

void AutoInfoWidget::onDateEditDiagnosticCard(network::ResponseShp)
{
}

void AutoInfoWidget::checkMileageButtonsVisible()
{
    if (_auto->_mileage == _ui->lineEditMileage->text().toULongLong())
    {
        _ui->btnSaveMileage->hide();
        _ui->btnDiscardMileage->hide();

        _ui->lineEditMileage->setFocus();
    }
    else
    {
        _ui->btnSaveMileage->show();
        _ui->btnDiscardMileage->show();
    }
}

void AutoInfoWidget::onChangeMileage(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();
    const auto& head = responseMap["head"].toMap();
    const auto& body = responseMap["body"].toMap();
    const auto& commandType = head["type"].toString();
    if (commandType.compare("ws_change_mileage") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const qint64 status = body["status"].toLongLong();
    if (status != 1)
    {
        qDebug() << "Does not save mileage";
    }
    else
    {
        _auto->_mileage = _ui->lineEditMileage->text().toULongLong();
    }

    checkMileageButtonsVisible();
}

void AutoInfoWidget::clearLayout()
{
    for (const auto& rWidget : _damageWidgets)
    {
        _damageClosedLayout->removeWidget(rWidget);
        _damageLayout->removeWidget(rWidget);
        rWidget->deleteLater();
    }
    _damageWidgets.clear();
}

void auto_review_workshop::AutoInfoWidget::on_dateEditInsurance(const QDate &date)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "ws_change_of_insurance_period";
    requestMap["id"] = _auto->_id;
    requestMap["date"] = _ui->dateEditInsurance->date().toString("yyyy-MM-dd");

    network::RequestShp request(new network::DefaultRequest("ws_change_of_insurance_period", requestMap));
    _requestsManager->execute(request, this, &AutoInfoWidget::onDateEditInsurance);
}

void AutoInfoWidget::on_dateEditDiagnosticCard(const QDate &date)
{
    QVariantMap requestMap;
    requestMap["type_command"] = "ws_change_diagnostic_card";
    requestMap["id"] = _auto->_id;
    requestMap["date"] = _ui->dateEditDiagnosticCard->date().toString("yyyy-MM-dd");

    network::RequestShp request(new network::DefaultRequest("ws_change_diagnostic_card", requestMap));
    _requestsManager->execute(request, this, &AutoInfoWidget::onDateEditDiagnosticCard);
}

void AutoInfoWidget::transferWidget(DamageWidget *damageWidget)
{
    _damageClosedLayout->removeWidget(damageWidget);
    _damageLayout->addWidget(damageWidget);
}

void AutoInfoWidget::transferWidgetClosed(DamageWidget *damageWidget)
{
    _damageLayout->removeWidget(damageWidget);
    _damageClosedLayout->addWidget(damageWidget);
}

void AutoInfoWidget::onMileageChanged()
{
    checkMileageButtonsVisible();
}

void AutoInfoWidget::onBtnSaveMileageClicked()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "ws_change_mileage";
    requestMap["id"] = _auto->_id;
    requestMap["mileage"] = _ui->lineEditMileage->text().toInt();

    network::RequestShp request(new network::DefaultRequest("ws_change_mileage", requestMap));
    _requestsManager->execute(request, this, &AutoInfoWidget::onChangeMileage);
}

void AutoInfoWidget::onBtnDiscardMileageClicked()
{
    _ui->lineEditMileage->setText(QString::number(_auto->_mileage));
}
