﻿#pragma once


namespace Ui
{
    class PermissionsWidget;
}

namespace auto_review_workshop
{

    class PermissionsWidget : public QFrame
    {
        Q_OBJECT

    public:
        explicit PermissionsWidget(QWidget* parent = nullptr);
        ~PermissionsWidget();

    private:
        void createFakeUsersModel();

    private:
        Ui::PermissionsWidget* _ui;
    };

}
