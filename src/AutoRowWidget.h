﻿#pragma once

#include "AutoObject.h"


namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;

    class Response;
    typedef QSharedPointer<Response> ResponseShp;
}

namespace session_controller
{
    class Session;
    typedef QSharedPointer<Session> SessionShp;
}

namespace Ui
{
    class AutoRowWidget;
}

namespace auto_review_workshop
{

    class AutoRowWidget : public QFrame
    {
        Q_OBJECT

    public:
        explicit AutoRowWidget(AutoObjectShp autoObj);
        ~AutoRowWidget();

        void showWidget();
        void hideWidget();
        bool matchByFilter(const QString &filter);

        inline quint64 getIdCar() { return _auto->_idCar; }
        inline AutoObjectShp getAutoObj() { return _auto; }

        void setSession(const session_controller::SessionShp &session);
        void setAutoObj(AutoObjectShp autoObj);

        void onToPark();

    public slots:
        void setFilterAndCheckVisible(const quint8 status = 1, const QString filterStr = "");

    signals:
        void openInfoClicked(AutoObjectShp);

    private slots:
        void onExpandClicked();
        void onOpenInfoClicked();
        void onToRepairClicked();
        void onToParkClicked();
        void onChBoxMaintenanceToggled(bool checked);

        void onResenMileageClicked();
        void onSaveMileageClicked();
        void onMileageChanged();

    private:
        void fillAllField();
        void checkButtons();
        void requestStatusCar();

        void onRequestStatusCar(network::ResponseShp response);
        void onRequestChangeMaintenance(network::ResponseShp response);

        void onChangeMileage(network::ResponseShp response);

    private:
        Ui::AutoRowWidget *_ui;

        AutoObjectShp _auto;
        network::RequestsManagerShp _requestsManager;
        session_controller::SessionShp _session;
    };

}
