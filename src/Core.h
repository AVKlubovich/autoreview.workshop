﻿#pragma once

#include "utils/BaseClasses/Singleton.h"


namespace network
{
    class RequestsManager;
    typedef QSharedPointer<RequestsManager> RequestsManagerShp;
}

namespace session_controller
{
    class Session;
    typedef QSharedPointer<Session> SessionShp;
}

namespace utils
{
    class Logger;
    typedef QSharedPointer<Logger> LoggerShp;
}

namespace auto_review_workshop
{

    class MainWindow;
    typedef QSharedPointer<MainWindow> MainWindowShp;

    class Core
        : public utils::Singleton<Core>
    {
    public:
        Core();
        ~Core() = default;

        bool init();
        void run();
        void done();

    private:
        void readConfig();
        bool initLogger();
        bool initWindow();
        bool initSession();

    private:
        utils::LoggerShp _logger;
        MainWindowShp _window;
        session_controller::SessionShp _session;
    };

}
