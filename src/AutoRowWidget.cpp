#include "Common.h"

#include "AutoRowWidget.h"
#include "ui_AutoRowWidget.h"

#include "network-core/RequestsManager/RequestsManager.h"
#include "network-core/RequestsManager/DefaultRequest.h"

#include "session-controller/Session.h"
#include "session-controller/SessionController.h"

using namespace auto_review_workshop;

AutoRowWidget::AutoRowWidget(AutoObjectShp autoObj)
    : QFrame(nullptr)
    , _ui(new Ui::AutoRowWidget)
    , _auto(autoObj)
    , _requestsManager(network::RequestsManagerShp::create())
{
    _ui->setupUi(this);

    _ui->widgetInfo->setVisible(false);

    connect(_ui->btnExpand, &QPushButton::clicked, this, &AutoRowWidget::onExpandClicked);
    connect(_ui->btnInfo, &QPushButton::clicked, this, &AutoRowWidget::onOpenInfoClicked);
    connect(_ui->btnToRepair, &QPushButton::clicked, this, &AutoRowWidget::onToRepairClicked);
    connect(_ui->btnToPark, &QPushButton::clicked, this, &AutoRowWidget::onToParkClicked);
    connect(_ui->chBoxMaintenance, &QCheckBox::toggled, this, &AutoRowWidget::onChBoxMaintenanceToggled);

    connect(_ui->btnResetMileage, &QPushButton::clicked, this, &AutoRowWidget::onResenMileageClicked);
    connect(_ui->btnSaveMileage, &QPushButton::clicked, this, &AutoRowWidget::onSaveMileageClicked);
    connect(_ui->lineEditMileage, &QLineEdit::textChanged, this, &AutoRowWidget::onMileageChanged);

    fillAllField();

    setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    setLineWidth(2);

    QRegExp mileageExp("[0-9]{,6}");
    QRegExpValidator *mileageValidator = new QRegExpValidator(mileageExp, _ui->lineEditMileage);
    _ui->lineEditMileage->setValidator(mileageValidator);
}

AutoRowWidget::~AutoRowWidget()
{
    delete _ui;
}

void AutoRowWidget::showWidget()
{
    this->show();
}

void AutoRowWidget::hideWidget()
{
    this->hide();
}

bool AutoRowWidget::matchByFilter(const QString &filter)
{
    if (filter.isEmpty())
    {
        return true;
    }

    return _auto->checkFilter(filter);
}

void AutoRowWidget::setSession(const session_controller::SessionShp &session)
{
    _session = session;
}

void AutoRowWidget::setAutoObj(AutoObjectShp autoObj)
{
    _auto = autoObj;
    fillAllField();
}

void AutoRowWidget::onToPark()
{
    onToParkClicked();
}

void AutoRowWidget::setFilterAndCheckVisible(const quint8 status, const QString filterStr)
{
    if (status != _auto->_status)
    {
        this->hide();
        return;
    }

    if (filterStr.isEmpty())
    {
        this->show();
        return;
    }

    if (!_auto->checkFilter(filterStr))
    {
        this->hide();
        return;
    }

    this->show();
}

void AutoRowWidget::onExpandClicked()
{
    if (_ui->widgetInfo->isVisible())
    {
        _ui->widgetInfo->setVisible(false);
        _ui->btnExpand->setText("+");
    }
    else
    {
        _ui->widgetInfo->setVisible(true);
        _ui->btnExpand->setText("-");
    }
}

void AutoRowWidget::onOpenInfoClicked()
{
    emit openInfoClicked(_auto);
}

void AutoRowWidget::onToRepairClicked()
{
    _auto->_status = 1;
    requestStatusCar();
}

void AutoRowWidget::onToParkClicked()
{
    _auto->_status = 3;
    requestStatusCar();
}

void AutoRowWidget::onChBoxMaintenanceToggled(bool checked)
{
    if (!checked)
        return;

    const QMessageBox::StandardButton response = QMessageBox::question(this,
        tr("Изменение значения"),
        tr("Изменить значение ТО обратно будет нельзя. Вы уверены, что хотите изменить значение ТО"));
    if (response == QMessageBox::No)
    {
        checkButtons();
        return;
    }

    _ui->chBoxMaintenance->setEnabled(false);

    QVariantMap requestMap;
    requestMap["type_command"] = "ws_change_maintenance";
    requestMap["id_car"] = _auto->_idCar;
    requestMap["mileage"] = _auto->_mileage;
    requestMap["is_big"] = 0;

    network::RequestShp request(new network::DefaultRequest("ws_change_maintenance", requestMap));
    _requestsManager->execute(request, this, &AutoRowWidget::onRequestChangeMaintenance);
}

void AutoRowWidget::onResenMileageClicked()
{
    _ui->lineEditMileage->setText(QString::number(_auto->_mileage));

    checkButtons();
}

void AutoRowWidget::onSaveMileageClicked()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "ws_change_mileage";
    requestMap["id"] = _auto->_id;
    requestMap["mileage"] = _ui->lineEditMileage->text().toInt();

    network::RequestShp request(new network::DefaultRequest("ws_change_mileage", requestMap));
    _requestsManager->execute(request, this, &AutoRowWidget::onChangeMileage);
}

void AutoRowWidget::onMileageChanged()
{
    checkButtons();
}

void AutoRowWidget::fillAllField()
{
    _ui->labelMake->setText(_auto->_marka);
    _ui->labelModel->setText(_auto->_model);
    _ui->labelNumber->setText(_auto->_number);
    _ui->labelDateCreate->setText(_auto->_dateCreate);
    _ui->lineEditMileage->setText(QString::number(_auto->_mileage));
    _ui->lineEditInsurance->setText(_auto->_insurance);
    _ui->lineEditDiagnosticCard->setText(_auto->_diagnosticCardEnd);
    _ui->lineEditMaintenance->setText(QString::number(_auto->_maintenance));

    checkButtons();
}

void AutoRowWidget::checkButtons()
{
    if (_auto->_status == 1)
    {
        _ui->btnToRepair->setVisible(false);
        _ui->btnToPark->setVisible(true);
    }
    else
    {
        _ui->btnToRepair->setVisible(true);
        _ui->btnToPark->setVisible(false);
    }

    if (_auto->_mileage == _ui->lineEditMileage->text().toUInt())
    {
        _ui->btnSaveMileage->hide();
        _ui->btnResetMileage->hide();
    }
    else
    {
        _ui->btnSaveMileage->show();
        _ui->btnResetMileage->show();
    }

    if (_auto->_mileage > _ui->lineEditMaintenance->text().toULongLong())
    {
        _ui->chBoxMaintenance->setEnabled(true);
        _ui->chBoxMaintenance->blockSignals(true);
        _ui->chBoxMaintenance->setChecked(false);
        _ui->chBoxMaintenance->blockSignals(false);
    }
    else if (_auto->_mileage == _ui->lineEditMaintenance->text().toULongLong())
    {
        _ui->chBoxMaintenance->setEnabled(false);
        _ui->chBoxMaintenance->blockSignals(true);
        _ui->chBoxMaintenance->setChecked(true);
        _ui->chBoxMaintenance->blockSignals(false);
    }
}

void AutoRowWidget::requestStatusCar()
{
    QVariantMap requestMap;
    requestMap["type_command"] = "ws_change_of_status_car";
    requestMap["id"] = _auto->_id;
    requestMap["id_car"] = _auto->_idCar;
    requestMap["status_car"] = _auto->_status;
    requestMap["login"] = _session->login();
    requestMap["password"] = _session->password();

    network::RequestShp request(new network::DefaultRequest("ws_change_of_status_car", requestMap));
    _requestsManager->execute(request, this, &AutoRowWidget::onRequestStatusCar);
}

void AutoRowWidget::onRequestStatusCar(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    checkButtons();
}

void AutoRowWidget::onRequestChangeMaintenance(network::ResponseShp response)
{
    if (response.isNull())
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        _ui->chBoxMaintenance->setEnabled(true);
        return;
    }

    const auto& responseMap = response->toVariant().toMap();
    const auto& head = responseMap["body"].toMap();
    const qint64 status = head["status"].toLongLong();
    if (status != 1)
    {
        qDebug() << "Does not change maintenance";
        _ui->chBoxMaintenance->setEnabled(true);
        return;
    }

    _ui->lineEditMaintenance->setText(_ui->lineEditMileage->text());
}

void AutoRowWidget::onChangeMileage(network::ResponseShp response)
{
    if (!response)
    {
        qDebug() << __FUNCTION__ << "Not valid response";
        return;
    }

    const auto& responseMap = response->toVariant().toMap();
    const auto& head = responseMap["head"].toMap();
    const auto& body = responseMap["body"].toMap();
    const auto& commandType = head["type"].toString();
    if (commandType.compare("ws_change_mileage") != 0)
    {
        qDebug() << __FUNCTION__ << "Unknown server error";
        return;
    }

    const qint64 status = body["status"].toLongLong();
    if (status != 1)
    {
        qDebug() << "Does not save mileage";
    }
    else
    {
        _auto->_mileage = _ui->lineEditMileage->text().toULongLong();
    }

    checkButtons();
}
