#include "Common.h"

#include "PermissionsWidget.h"
#include "ui_PermissionsWidget.h"


using namespace auto_review_workshop;

PermissionsWidget::PermissionsWidget(QWidget* parent)
    : QFrame(parent)
    , _ui(new Ui::PermissionsWidget)
{
    _ui->setupUi(this);

    createFakeUsersModel();
}

PermissionsWidget::~PermissionsWidget()
{
    delete _ui;
}

void PermissionsWidget::createFakeUsersModel()
{
    QStandardItemModel* model = new QStandardItemModel();

    QStandardItem* parentItem1 = new QStandardItem("Администратор");
    model->appendRow(parentItem1);

    QStandardItem* parentItem2 = new QStandardItem("Автопарк");
    model->appendRow(parentItem2);

    QStandardItem* parentItem3 = new QStandardItem("Рем. зона");
    model->appendRow(parentItem3);

    _ui->treeUsersList->setModel(model);
}
