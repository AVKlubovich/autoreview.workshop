﻿#pragma once


namespace auto_review_workshop
{

    class CheckableComboBox : public QComboBox
    {
        Q_OBJECT

    public:
        explicit CheckableComboBox(QWidget* parent = nullptr);
        ~CheckableComboBox();

        QVariantList selectedUserData() const;
        void selectUserData(const QVariantList &data);
        QModelIndexList checkedIndexes() const;
        QList <QVariant> realIndex();
        void clearSelectIndex();

    signals:
        void selectedUserDataChanged(const QVariantList &data);
        void done();
        void sClickedCheckedComboBox();

    private slots:
        void onDataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight, const QVector<int> &roles);

    public slots:
        void updateTextHints();

    private:
        bool eventFilter(QObject* obj, QEvent* e) override;
        void paintEvent(QPaintEvent* event) override;

    private:
        QString _textHint;
    };

}
