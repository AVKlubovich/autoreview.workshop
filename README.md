# AutoReview-workshop

##Описание проекта
Клиент для ремонтника.

##Используемые технологии
[C++14](https://ru.wikipedia.org/wiki/C%2B%2B14)  
[Qt](https://ru.wikipedia.org/wiki/Qt)

##Системные требования
* Windows; Linux
* Qt5.5 >

##Документация
[Соглашения по оформлению кода](https://drive.google.com/open?id=0B48GpktEZIksWjNjTmdWcW53Rnc)

##Список контрибьюторов 
[Борода](../../../../avklubovich)  
[Мастер йода и зелёнки](../../../../jimak)