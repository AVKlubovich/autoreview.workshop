﻿-- SQL Manager Lite for PostgreSQL 5.7.1.47382
-- ---------------------------------------
-- Хост         : 192.168.202.222
-- База данных  : auto_review_db
-- Версия       : PostgreSQL 9.5.0, compiled by Visual C++ build 1800, 64-bit



SET check_function_bodies = false;
--
-- Structure for table tires_type (OID = 31830) : 
--
SET search_path = public, pg_catalog;
CREATE TABLE public.tires_type (
    id integer DEFAULT nextval('rubber_type_id_seq'::regclass) NOT NULL,
    type text
)
WITH (oids = false);
--
-- Structure for table car_damage (OID = 31857) : 
--
CREATE TABLE public.car_damage (
    id bigint DEFAULT nextval('"car_вamage_id_seq"'::regclass) NOT NULL,
    id_car bigint,
    id_element_damage integer,
    type_damage integer,
    comment text,
    date_create timestamp(0) without time zone,
    status boolean DEFAULT false,
    date_close timestamp(0) without time zone
)
WITH (oids = false);
--
-- Structure for table photos (OID = 31868) : 
--
CREATE TABLE public.photos (
    id bigserial NOT NULL,
    id_car_damage bigint,
    url text,
    date_create_photo timestamp(0) without time zone
)
WITH (oids = false);
--
-- Structure for table car_accessories (OID = 31879) : 
--
CREATE TABLE public.car_accessories (
    id bigserial NOT NULL,
    id_accessory bigint,
    id_car bigint,
    status integer DEFAULT 1,
    date_update date,
    comment text
)
WITH (oids = false);
--
-- Structure for table accessories (OID = 31890) : 
--
CREATE TABLE public.accessories (
    id bigserial NOT NULL,
    accessory text,
    type integer NOT NULL
)
WITH (oids = false);
--
-- Structure for table users (OID = 31901) : 
--
CREATE TABLE public.users (
    id bigserial NOT NULL,
    name text
)
WITH (oids = false);
--
-- Structure for table config_elements_of_cars (OID = 31912) : 
--
CREATE TABLE public.config_elements_of_cars (
    id bigint DEFAULT nextval('elements_of_cars_id_seq'::regclass) NOT NULL,
    element text,
    color text
)
WITH (oids = false);
--
-- Structure for table type_damage_cars (OID = 31924) : 
--
CREATE TABLE public.type_damage_cars (
    id bigserial NOT NULL,
    type text
)
WITH (oids = false);
--
-- Structure for table check_type (OID = 31935) : 
--
CREATE TABLE public.check_type (
    id bigserial NOT NULL,
    type text NOT NULL
)
WITH (oids = false);
--
-- Structure for table damage_from_elements (OID = 31946) : 
--
CREATE TABLE public.damage_from_elements (
    id bigserial NOT NULL,
    id_element bigint,
    id_damage bigint
)
WITH (oids = false);
--
-- Structure for table config_car_positions (OID = 31957) : 
--
CREATE TABLE public.config_car_positions (
    id bigserial NOT NULL,
    foreground_url text,
    background_url text
)
WITH (oids = false);
--
-- Structure for table config_elements_for_car_positions (OID = 31981) : 
--
CREATE TABLE public.config_elements_for_car_positions (
    id bigserial NOT NULL,
    id_car_position bigint,
    id_car_element bigint,
    "axis_X" integer,
    "axis_Y" integer
)
WITH (oids = false);
--
-- Structure for table maintenance (OID = 31991) : 
--
CREATE TABLE public.maintenance (
    id serial NOT NULL,
    id_car bigint,
    mileage bigint,
    is_big integer DEFAULT 0,
    date_create timestamp without time zone
)
WITH (oids = false);
--
-- Structure for table drivers (OID = 32006) : 
--
CREATE TABLE public.drivers (
    id bigint NOT NULL,
    pin bigint NOT NULL,
    date_last_change timestamp without time zone
)
WITH (oids = false);
--
-- Structure for table statuses (OID = 41871) : 
--
CREATE TABLE public.statuses (
    id bigserial NOT NULL,
    name text
)
WITH (oids = false);
--
-- Structure for table info_about_cars (OID = 41890) : 
--
CREATE TABLE public.info_about_cars (
    id serial NOT NULL,
    id_car bigint NOT NULL,
    id_park integer,
    insurance_end date,
    diagnostic_card_end date,
    id_tire integer,
    date_create timestamp(0) without time zone,
    mileage bigint,
    osago_date date,
    osago_number bigint,
    pts text,
    srts text,
    tire_marka text,
    diagnostic_card_data text,
    child_restraint_means text,
    status integer DEFAULT 0
)
WITH (oids = false);
--
-- Data for table public.tires_type (OID = 31830) (LIMIT 0,3)
--
INSERT INTO tires_type (id, type)
VALUES (1, 'Летняя резина');

INSERT INTO tires_type (id, type)
VALUES (2, 'Зимняя резина');

INSERT INTO tires_type (id, type)
VALUES (3, 'Всесезонная резина');

--
-- Data for table public.car_damage (OID = 31857) (LIMIT 0,26)
--
INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (121, 4, 23, 1, NULL, '2017-02-13 08:59:46', false, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (122, 1, 13, 4, NULL, '2017-02-27 15:00:42', false, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (112, 1, 5, 1, NULL, '2017-02-10 13:58:06', false, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (110, 1, 22, 5, NULL, '2017-02-10 13:47:55', false, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (120, 1, 15, 1, NULL, '2017-02-13 08:44:38', true, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (105, 1, 26, 1, NULL, '2017-02-10 12:32:18', true, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (114, 1, 8, 1, NULL, '2017-02-10 14:05:00', true, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (116, 1, 7, 3, NULL, '2017-02-10 14:09:24', true, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (107, 1, 27, 3, NULL, '2017-02-10 12:44:51', true, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (124, 10261, 16, 1, NULL, '2017-04-29 13:26:00', false, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (126, 10261, 1, 2, NULL, '2017-04-29 14:28:11', true, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (127, 10261, 1, 2, NULL, '2017-04-29 14:29:58', false, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (128, 10261, 1, 2, NULL, '2017-04-29 14:37:34', false, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (125, 10261, 1, 2, NULL, '2017-04-29 15:41:31', false, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (123, 1, 16, 1, NULL, '2017-05-04 09:38:26', true, '2017-05-04 09:38:26');

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (119, 1, 1, 1, NULL, '2017-05-04 09:40:36', true, '2017-05-04 09:40:36');

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (111, 1, 21, 1, NULL, '2017-05-04 09:41:29', true, '2017-05-04 09:41:29');

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (103, 1, 26, 1, NULL, '2017-05-04 09:48:09', false, '2017-05-04 09:48:09');

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (109, 1, 19, 4, NULL, '2017-05-04 09:48:10', false, '2017-05-04 09:48:10');

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (108, 1, 27, 3, NULL, '2017-05-04 09:48:32', true, '2017-05-04 09:48:32');

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (118, 1, 20, 10, NULL, '2017-05-04 09:50:30', true, '2017-05-04 09:50:30');

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (104, 1, 26, 1, NULL, '2017-02-10 12:25:49', false, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (106, 1, 27, 3, NULL, '2017-02-10 12:32:18', false, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (113, 1, 30, 4, NULL, '2017-02-10 14:00:14', false, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (115, 1, 10, 1, NULL, '2017-02-10 14:06:55', false, NULL);

INSERT INTO car_damage (id, id_car, id_element_damage, type_damage, comment, date_create, status, date_close)
VALUES (117, 1, 9, 2, NULL, '2017-02-10 14:09:24', false, NULL);

--
-- Data for table public.photos (OID = 31868) (LIMIT 0,157)
--
INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (74, 68, 'http://192.168.105.130/files/2017-01-18_08-56-18-5077060.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (15, 178, 'test', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (17, 30, 'http://192.168.105.130/files/2017-01-09_09-19-53-5282340.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (18, 30, 'http://192.168.105.130/files/2017-01-09_09-19-53-5292820.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (19, 31, 'http://192.168.105.130/files/2017-01-09_09-31-52-9910290.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (20, 31, 'http://192.168.105.130/files/2017-01-09_09-31-52-9924420.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (21, 32, 'http://192.168.105.130/files/2017-01-10_10-12-30-0982330.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (22, 33, 'http://192.168.105.130/files/2017-01-10_10-15-10-4549580.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (23, 34, 'http://192.168.105.130/files/2017-01-11_09-18-11-5404450.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (24, 35, 'http://192.168.105.130/files/2017-01-11_11-03-55-3345730.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (25, 36, 'http://192.168.105.130/files/2017-01-11_13-10-40-3555260.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (26, 36, 'http://192.168.105.130/files/2017-01-11_13-10-40-3567340.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (27, 37, 'http://192.168.105.130/files/2017-01-11_13-48-58-9987940.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (28, 37, 'http://192.168.105.130/files/2017-01-11_13-48-58-9998730.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (29, 38, 'http://192.168.105.130/files/2017-01-11_13-59-05-2742800.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (30, 39, 'http://192.168.105.130/files/2017-01-12_07-46-19-4611710.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (31, 39, 'http://192.168.105.130/files/2017-01-12_07-46-19-4626320.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (32, 40, 'http://192.168.105.130/files/2017-01-12_07-47-37-5837650.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (33, 41, 'http://192.168.105.130/files/2017-01-12_08-12-46-0026580.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (34, 42, 'http://192.168.105.130/files/2017-01-12_10-38-53-0017150.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (35, 43, 'http://192.168.105.130/files/2017-01-12_12-00-48-7823930.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (36, 43, 'http://192.168.105.130/files/2017-01-12_12-00-48-7832450.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (37, 44, 'http://192.168.105.130/files/2017-01-12_12-00-48-7839450.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (38, 45, 'http://192.168.105.130/files/2017-01-13_08-32-47-2300760.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (39, 46, 'http://192.168.105.130/files/2017-01-17_08-54-23-3254230.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (40, 46, 'http://192.168.105.130/files/2017-01-17_08-54-23-3267480.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (41, 47, 'http://192.168.105.130/files/2017-01-17_11-08-28-5531200.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (42, 47, 'http://192.168.105.130/files/2017-01-17_11-08-28-5570460.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (43, 48, 'http://192.168.105.130/files/2017-01-17_11-35-23-8367720.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (44, 49, 'http://192.168.105.130/files/2017-01-17_11-37-36-4649940.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (45, 50, 'http://192.168.105.130/files/2017-01-17_11-37-39-6372270.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (46, 51, 'http://192.168.105.130/files/2017-01-17_12-14-47-6356640.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (47, 51, 'http://192.168.105.130/files/2017-01-17_12-14-47-6432400.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (48, 52, 'http://192.168.105.130/files/2017-01-17_12-14-50-1140310.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (49, 52, 'http://192.168.105.130/files/2017-01-17_12-14-50-1192010.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (50, 53, 'http://192.168.105.130/files/2017-01-17_13-33-53-9720610.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (51, 54, 'http://192.168.105.130/files/2017-01-17_13-35-09-5716130.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (52, 55, 'http://192.168.105.130/files/2017-01-17_13-36-00-6651760.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (53, 56, 'http://192.168.105.130/files/2017-01-17_15-56-59-0169130.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (54, 56, 'http://192.168.105.130/files/2017-01-17_15-56-59-0177550.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (55, 57, 'http://192.168.105.130/files/2017-01-18_08-37-43-0208640.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (56, 58, 'http://192.168.105.130/files/2017-01-18_08-38-58-5007700.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (57, 58, 'http://192.168.105.130/files/2017-01-18_08-38-58-5050930.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (58, 59, 'http://192.168.105.130/files/2017-01-18_08-38-58-5083260.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (59, 60, 'http://192.168.105.130/files/2017-01-18_08-39-23-1612130.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (60, 61, 'http://192.168.105.130/files/2017-01-18_08-41-06-2666010.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (61, 61, 'http://192.168.105.130/files/2017-01-18_08-41-06-2699480.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (62, 62, 'http://192.168.105.130/files/2017-01-18_08-41-08-0495140.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (63, 62, 'http://192.168.105.130/files/2017-01-18_08-41-08-0522630.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (64, 63, 'http://192.168.105.130/files/2017-01-18_08-41-08-1718700.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (65, 63, 'http://192.168.105.130/files/2017-01-18_08-41-08-1747160.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (66, 64, 'http://192.168.105.130/files/2017-01-18_08-41-22-2691740.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (67, 64, 'http://192.168.105.130/files/2017-01-18_08-41-22-2719280.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (68, 65, 'http://192.168.105.130/files/2017-01-18_08-54-38-6115910.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (69, 65, 'http://192.168.105.130/files/2017-01-18_08-54-38-6139620.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (70, 66, 'http://192.168.105.130/files/2017-01-18_08-54-45-2873880.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (71, 66, 'http://192.168.105.130/files/2017-01-18_08-54-45-2897150.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (72, 67, 'http://192.168.105.130/files/2017-01-18_08-54-48-4361340.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (73, 67, 'http://192.168.105.130/files/2017-01-18_08-54-48-4384330.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (75, 69, 'http://192.168.105.130/files/2017-01-18_09-02-18-8698540.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (76, 69, 'http://192.168.105.130/files/2017-01-18_09-02-18-8727740.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (77, 70, 'http://192.168.105.130/files/2017-01-18_09-02-28-5173480.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (78, 70, 'http://192.168.105.130/files/2017-01-18_09-02-28-5196130.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (79, 71, 'http://192.168.105.130/files/2017-01-18_09-02-29-3758100.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (80, 71, 'http://192.168.105.130/files/2017-01-18_09-02-29-3779840.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (81, 72, 'http://192.168.105.130/files/2017-01-18_09-02-44-0818360.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (82, 72, 'http://192.168.105.130/files/2017-01-18_09-02-44-0845090.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (83, 73, 'http://192.168.105.130/files/2017-01-18_09-02-45-7764590.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (84, 73, 'http://192.168.105.130/files/2017-01-18_09-02-45-7788250.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (85, 74, 'http://192.168.105.130/files/2017-01-18_09-03-01-3045020.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (86, 74, 'http://192.168.105.130/files/2017-01-18_09-03-01-3074090.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (87, 75, 'http://192.168.105.130/files/2017-01-18_09-03-23-4779410.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (88, 75, 'http://192.168.105.130/files/2017-01-18_09-03-23-4804990.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (89, 76, 'http://192.168.105.130/files/2017-01-18_09-03-23-7203830.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (90, 76, 'http://192.168.105.130/files/2017-01-18_09-03-23-7228320.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (91, 77, 'http://192.168.105.130/files/2017-01-18_09-44-58-9209580.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (92, 78, 'http://192.168.105.130/files/2017-01-18_10-38-42-5799860.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (93, 79, 'http://192.168.105.130/files/2017-01-23_08-33-56-9313030.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (94, 79, 'http://192.168.105.130/files/2017-01-23_08-33-56-9322510.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (95, 80, 'http://192.168.105.130/files/2017-01-23_11-18-51-3693860.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (96, 81, 'http://192.168.105.130/files/2017-01-24_18-48-26-8650430.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (97, 82, 'http://192.168.105.130/files/2017-01-27_10-24-31-5073660.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (98, 83, 'http://192.168.105.130/files/2017-01-30_12-30-59-5198240.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (99, 84, 'http://192.168.105.130/files/2017-01-30_12-35-07-1783640.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (100, 85, 'http://192.168.105.130/files/2017-01-31_12-32-01-68189501_photo', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (101, 85, 'http://192.168.105.130/files/2017-01-31_12-32-01-68285402_photo', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (102, 86, 'http://192.168.105.130/files/2017-02-01_12-11-05-3617380.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (103, 86, 'http://192.168.105.130/files/2017-02-01_12-11-05-3630540.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (104, 87, 'http://192.168.105.130/files/2017-02-01_12-11-05-3642750.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (105, 88, 'http://192.168.105.130/files/2017-02-03_11-45-51-3699020.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (106, 88, 'http://192.168.105.130/files/2017-02-03_11-45-51-3717790.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (107, 89, 'http://192.168.105.130/files/2017-02-08_06-16-23-6494410.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (108, 89, 'http://192.168.105.130/files/2017-02-08_06-16-23-6504010.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (109, 90, 'http://192.168.105.130/files/2017-02-08_06-18-19-9182650.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (110, 90, 'http://192.168.105.130/files/2017-02-08_06-18-19-9192630.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (111, 90, 'http://192.168.105.130/files/2017-02-08_06-18-19-9200730.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (112, 91, 'http://192.168.105.130/files/2017-02-08_06-23-02-0806270.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (113, 91, 'http://192.168.105.130/files/2017-02-08_06-23-02-0826320.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (114, 92, 'http://192.168.105.130/files/2017-02-08_06-29-04-8545690.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (115, 93, 'http://192.168.105.130/files/2017-02-08_10-30-37-2315370.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (116, 93, 'http://192.168.105.130/files/2017-02-08_10-30-37-2405400.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (117, 93, 'http://192.168.105.130/files/2017-02-08_10-30-37-2481170.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (118, 94, 'http://192.168.105.130/files/2017-02-09_16-55-53-8986320.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (119, 95, 'http://192.168.105.130/files/2017-02-10_06-52-09-2299230.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (120, 96, 'http://192.168.105.130/files/2017-02-10_07-08-42-8390670.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (121, 97, 'http://192.168.105.130/files/2017-02-10_08-11-19-8623330.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (122, 98, 'http://192.168.105.130/files/2017-02-10_08-29-18-5569850.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (123, 99, 'http://192.168.105.130/files/2017-02-10_08-31-13-2807200.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (124, 100, 'http://192.168.105.130/files/2017-02-10_08-32-39-8852310.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (125, 101, 'http://192.168.105.130/files/2017-02-10_08-34-12-2011120.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (126, 102, 'http://192.168.105.130/files/2017-02-10_08-45-29-5841160.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (127, 102, 'http://192.168.105.130/files/2017-02-10_08-45-29-5925710.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (128, 103, 'http://192.168.105.130/files/2017-02-10_09-23-59-6385940.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (129, 103, 'http://192.168.105.130/files/2017-02-10_09-23-59-6468910.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (130, 103, 'http://192.168.105.130/files/2017-02-10_09-23-59-6570160.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (131, 103, 'http://192.168.105.130/files/2017-02-10_09-23-59-6675650.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (132, 104, 'http://192.168.105.130/files/2017-02-10_09-25-44-1297190.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (133, 105, 'http://192.168.105.130/files/2017-02-10_09-32-13-5424320.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (134, 106, 'http://192.168.105.130/files/2017-02-10_09-32-13-5504370.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (135, 107, 'http://192.168.105.130/files/2017-02-10_09-44-45-7723020.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (136, 108, 'http://192.168.105.130/files/2017-02-10_09-55-37-2536160.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (137, 106, 'http://192.168.105.130/files/2017-02-10_10-38-03-9372600.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (138, 109, 'http://192.168.105.130/files/2017-02-10_10-44-06-4400080.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (139, 109, 'http://192.168.105.130/files/2017-02-10_10-44-06-4515150.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (140, 103, 'http://192.168.105.130/files/2017-02-10_10-44-06-4604190.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (141, 109, 'http://192.168.105.130/files/2017-02-10_10-45-39-2943950.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (142, 109, 'http://192.168.105.130/files/2017-02-10_10-47-50-3463980.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (143, 109, 'http://192.168.105.130/files/2017-02-10_10-47-50-3573990.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (144, 110, 'http://192.168.105.130/files/2017-02-10_10-47-50-3704640.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (145, 110, 'http://192.168.105.130/files/2017-02-10_10-47-50-3809070.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (146, 111, 'http://192.168.105.130/files/2017-02-10_10-47-50-3890860.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (147, 111, 'http://192.168.105.130/files/2017-02-10_10-47-50-3965310.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (148, 112, 'http://192.168.105.130/files/2017-02-10_10-58-00-8558960.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (149, 103, 'http://192.168.105.130/files/2017-02-10_10-58-00-8621720.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (150, 113, 'http://192.168.105.130/files/2017-02-10_11-00-09-2576860.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (151, 114, 'http://192.168.105.130/files/2017-02-10_11-04-55-4512420.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (152, 114, 'http://192.168.105.130/files/2017-02-10_11-04-55-4589940.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (153, 115, 'http://192.168.105.130/files/2017-02-10_11-06-50-4863420.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (154, 116, 'http://192.168.105.130/files/2017-02-10_11-09-18-7603680.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (155, 117, 'http://192.168.105.130/files/2017-02-10_11-09-18-7680020.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (156, 117, 'http://192.168.105.130/files/2017-02-10_11-09-18-7758900.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (157, 113, 'http://192.168.105.130/files/2017-02-10_11-09-18-7834250.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (158, 118, 'http://192.168.105.130/files/2017-02-10_11-32-29-6372690.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (159, 118, 'http://192.168.105.130/files/2017-02-10_11-32-29-6393360.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (160, 119, 'http://192.168.105.130/files/2017-02-13_05-37-40-2904610.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (161, 119, 'http://192.168.105.130/files/2017-02-13_05-37-40-2975000.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (162, 120, 'http://192.168.105.130/files/2017-02-13_05-44-37-3052780.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (163, 121, 'http://192.168.105.130/files/2017-02-13_05-59-45-9350200.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (164, 122, 'http://192.168.105.130/files/2017-02-27_12-00-40-1703550.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (165, 123, 'http://192.168.105.130/files/2017-04-29_10-07-48-6969460.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (166, 124, 'http://192.168.105.130/files/2017-04-29_10-25-56-5766330.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (167, 125, 'http://192.168.105.130/files/2017-04-29_10-32-47-7517970.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (168, 126, 'http://192.168.105.130/files/2017-04-29_11-25-14-4851770.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (169, 127, 'http://192.168.105.130/files/2017-04-29_11-29-53-9056290.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (170, 128, 'http://192.168.105.130/files/2017-04-29_11-37-30-2414210.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (171, 128, 'http://192.168.105.130/files/2017-04-29_11-37-30-2456870.jpg', NULL);

INSERT INTO photos (id, id_car_damage, url, date_create_photo)
VALUES (172, 125, 'http://192.168.105.130/files/2017-04-29_11-49-44-0066040.jpg', NULL);

--
-- Data for table public.car_accessories (OID = 31879) (LIMIT 0,47)
--
INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (48, 43, 1, 2, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (49, 44, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (50, 45, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (51, 46, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (52, 47, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (2, 15, 1, 2, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (3, 16, 1, 1, '2017-02-07', 'jki');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (5, 17, 1, 3, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (6, 18, 1, 3, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (7, 19, 1, 1, '2017-02-07', 'кто-то отдолжиддддддддддддддддддддддддддддддддддддддддддддддддддддд');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (8, 20, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (9, 21, 1, 1, '2017-02-07', 'все ок');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (10, 22, 1, 2, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (11, 14, 1, 1, '2017-02-07', 'нет');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (14, 13, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (15, 12, 1, 2, '2017-02-07', 'нет');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (16, 11, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (17, 10, 1, 2, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (18, 9, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (19, 8, 1, 3, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (20, 7, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (21, 6, 1, 2, '2017-02-07', 'нет');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (22, 5, 1, 3, '2017-02-07', 'вернули');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (23, 4, 1, 3, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (24, 3, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (25, 2, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (26, 1, 1, 2, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (27, 23, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (40, 35, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (41, 36, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (44, 39, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (28, 24, 1, 3, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (29, 25, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (30, 26, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (31, 27, 1, 2, '2017-02-07', 'нет');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (32, 28, 1, 2, '2017-02-07', 'нет');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (33, 29, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (34, 30, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (42, 37, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (43, 38, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (35, 31, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (36, 32, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (37, 33, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (39, 34, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (45, 40, 1, 1, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (46, 41, 1, 3, '2017-02-07', '');

INSERT INTO car_accessories (id, id_accessory, id_car, status, date_update, comment)
VALUES (47, 42, 1, 3, '2017-02-07', '');

--
-- Data for table public.accessories (OID = 31890) (LIMIT 0,50)
--
INSERT INTO accessories (id, accessory, type)
VALUES (32, 'POS терминал для оплаты картой (наличие)', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (1, '4 одинаковых колеса + клеймкение', 1);

INSERT INTO accessories (id, accessory, type)
VALUES (33, 'Wi-Fi роутер (наличие)', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (2, 'Колпаки на колёсах', 1);

INSERT INTO accessories (id, accessory, type)
VALUES (3, 'Колпак такси', 1);

INSERT INTO accessories (id, accessory, type)
VALUES (4, 'Тряпка в тубусе, в двери', 2);

INSERT INTO accessories (id, accessory, type)
VALUES (5, 'Держатель смартфона', 2);

INSERT INTO accessories (id, accessory, type)
VALUES (34, 'Инструкция по экспл. а/м', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (6, 'Накидка на вод. кресло', 2);

INSERT INTO accessories (id, accessory, type)
VALUES (7, 'Коврики салона (кол-во шт.)', 2);

INSERT INTO accessories (id, accessory, type)
VALUES (8, 'Коврик багажника', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (9, 'Запасное колесо(исправное) + клеймение', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (10, 'Домкрат с ручкой', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (11, 'Ключ балонный', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (35, 'Планшет Ф-А5 с креплением + квитации на курьерскую доставку', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (12, 'Крюк буксировачный', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (13, 'Знак аварийной остановки', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (14, 'Огнетушитель', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (15, 'Аптечка', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (16, 'Трос буксировачный', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (17, 'Адапт. для ремн. «ФЭСТ»', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (18, 'Детское у\\устр. БУСТЕР', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (19, 'Детское кресло (0-13)кг.', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (20, 'Детское кресло (9-36)кг.', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (21, 'Табличка для встречи', 3);

INSERT INTO accessories (id, accessory, type)
VALUES (22, 'Визитница большая', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (23, 'Визитница малая', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (24, 'Карман для рекл. продукции', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (25, 'Маркер', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (26, 'Шнур USB универсальный Пластмас.', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (27, 'Шнур micro usb (кол-во шт)', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (28, 'Шнур iPhone 4 (кол-во шт)', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (29, 'Шнур iPhone 5 (кол-во шт)', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (30, 'Переходное USB в прикур. Метал.', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (36, 'Конверт с документами', 5);

INSERT INTO accessories (id, accessory, type)
VALUES (37, 'Свид-во о рег.трансп. Ср-ва', 5);

INSERT INTO accessories (id, accessory, type)
VALUES (38, 'Полис ОСАГО', 5);

INSERT INTO accessories (id, accessory, type)
VALUES (39, 'Диагностическая карта т/с', 5);

INSERT INTO accessories (id, accessory, type)
VALUES (40, 'Сертификат соотв.на колпак', 5);

INSERT INTO accessories (id, accessory, type)
VALUES (41, 'Договор публичной оферты', 5);

INSERT INTO accessories (id, accessory, type)
VALUES (42, 'Извещение о ДТП (с памяткой)', 5);

INSERT INTO accessories (id, accessory, type)
VALUES (43, 'Акт приема-передачи автомобиля в ремзону', 5);

INSERT INTO accessories (id, accessory, type)
VALUES (44, 'Правила перев.пасс.', 5);

INSERT INTO accessories (id, accessory, type)
VALUES (50, 'АКБ с клеймом', 6);

INSERT INTO accessories (id, accessory, type)
VALUES (45, 'Памятка водителю при ДТП ', 5);

INSERT INTO accessories (id, accessory, type)
VALUES (31, 'Переходное USB в прикур.', 4);

INSERT INTO accessories (id, accessory, type)
VALUES (46, 'Смартфон с программой', 5);

INSERT INTO accessories (id, accessory, type)
VALUES (47, 'Целостность смартфона', 5);

INSERT INTO accessories (id, accessory, type)
VALUES (48, 'Лицензия на автомобиль', 6);

INSERT INTO accessories (id, accessory, type)
VALUES (49, 'Шторка на заднем стекле (наличие и целостность)', 6);

--
-- Data for table public.users (OID = 31901) (LIMIT 0,1)
--
INSERT INTO users (id, name)
VALUES (2797, 'test_andrej');

--
-- Data for table public.config_elements_of_cars (OID = 31912) (LIMIT 0,30)
--
INSERT INTO config_elements_of_cars (id, element, color)
VALUES (2, 'Заднее стекло', '#52eb15');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (3, 'Левое заднее колесо', '#1feb15');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (4, 'Лобовое стекло', '#a2eb15');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (5, 'Багажник', '#feed00');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (6, 'Левое переднее колесо', '#7f15eb');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (7, 'Левое переднее крыло', '#1a15eb');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (8, 'Левое заднее крыло', '#eb156b');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (9, 'Левая передняя дверь', '#bb15eb');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (10, 'Левая задняя дверь', '#eb15b5');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (11, 'Левое переднее стекло', '#1584eb');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (12, 'Левое заднее стекло', '#15b1eb');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (13, 'Левое зеркало', '#5615eb');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (14, 'Левая фара', '#4c15eb');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (15, 'Левый фонарь', '#eb152e');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (16, 'Передний бампер', '#1538eb');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (17, 'Задний бампер', '#eb159d');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (18, 'Крыша', '#157feb');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (19, 'Правый фонарь', '#15b6eb');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (20, 'Правая фара', '#15d4eb');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (21, 'Правое заднее колесо', '#15e8eb');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (22, 'Правое переднее колесо', '#15ebde');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (23, 'Правое заднее крыло', '#15ebc0');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (24, 'Правое переднее крыло', '#15eba7');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (25, 'Правое зеркало', '#15eb7f');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (26, 'Правая задняя дверь', '#15eb61');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (27, 'Правая передняя дверь', '#15eb3d');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (28, 'Правое заднее стекло', '#15eb24');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (29, 'Правое переднее стекло', '#24eb15');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (30, 'Шашки', '#eb3815');

INSERT INTO config_elements_of_cars (id, element, color)
VALUES (1, 'Капот', '#1561eb');

--
-- Data for table public.type_damage_cars (OID = 31924) (LIMIT 0,10)
--
INSERT INTO type_damage_cars (id, type)
VALUES (2, 'Царапина');

INSERT INTO type_damage_cars (id, type)
VALUES (1, 'Вмятина');

INSERT INTO type_damage_cars (id, type)
VALUES (3, 'Скол');

INSERT INTO type_damage_cars (id, type)
VALUES (4, 'Разбито');

INSERT INTO type_damage_cars (id, type)
VALUES (5, 'Порез');

INSERT INTO type_damage_cars (id, type)
VALUES (6, 'Отсутствует');

INSERT INTO type_damage_cars (id, type)
VALUES (7, 'Прокол');

INSERT INTO type_damage_cars (id, type)
VALUES (8, 'Порвано');

INSERT INTO type_damage_cars (id, type)
VALUES (9, 'Пятна');

INSERT INTO type_damage_cars (id, type)
VALUES (10, 'Трещина');

--
-- Data for table public.check_type (OID = 31935) (LIMIT 0,5)
--
INSERT INTO check_type (id, type)
VALUES (1, 'Проверка авто с наружи');

INSERT INTO check_type (id, type)
VALUES (2, 'Проверка с водительской стороны');

INSERT INTO check_type (id, type)
VALUES (3, 'Проверка багажника');

INSERT INTO check_type (id, type)
VALUES (5, 'Проверка конверта с док-ми на авто');

INSERT INTO check_type (id, type)
VALUES (4, 'Проверка бардачка');

--
-- Data for table public.damage_from_elements (OID = 31946) (LIMIT 0,9)
--
INSERT INTO damage_from_elements (id, id_element, id_damage)
VALUES (1, 3, 7);

INSERT INTO damage_from_elements (id, id_element, id_damage)
VALUES (2, 1, 1);

INSERT INTO damage_from_elements (id, id_element, id_damage)
VALUES (3, 1, 2);

INSERT INTO damage_from_elements (id, id_element, id_damage)
VALUES (4, 1, 3);

INSERT INTO damage_from_elements (id, id_element, id_damage)
VALUES (5, 2, 2);

INSERT INTO damage_from_elements (id, id_element, id_damage)
VALUES (6, 2, 3);

INSERT INTO damage_from_elements (id, id_element, id_damage)
VALUES (7, 2, 4);

INSERT INTO damage_from_elements (id, id_element, id_damage)
VALUES (8, 3, 5);

INSERT INTO damage_from_elements (id, id_element, id_damage)
VALUES (9, 3, 6);

--
-- Data for table public.config_car_positions (OID = 31957) (LIMIT 0,5)
--
INSERT INTO config_car_positions (id, foreground_url, background_url)
VALUES (3, 'http://192.168.105.130/files/config/octavia_right.png', 'http://192.168.105.130/files/config/octavia_right_background.png');

INSERT INTO config_car_positions (id, foreground_url, background_url)
VALUES (4, 'http://192.168.105.130/files/config/octavia_back.png', 'http://192.168.105.130/files/config/octavia_back_background.png');

INSERT INTO config_car_positions (id, foreground_url, background_url)
VALUES (5, 'http://192.168.105.130/files/config/octavia_up.png', 'http://192.168.105.130/files/config/octavia_up_background.png');

INSERT INTO config_car_positions (id, foreground_url, background_url)
VALUES (1, 'http://192.168.105.130/files/config/octavia_front.png', 'http://192.168.105.130/files/config/octavia_front_background.png');

INSERT INTO config_car_positions (id, foreground_url, background_url)
VALUES (2, 'http://192.168.105.130/files/config/octavia_left.png', 'http://192.168.105.130/files/config/octavia_left_background.png');

--
-- Data for table public.config_elements_for_car_positions (OID = 31981) (LIMIT 0,58)
--
INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (47, 3, 20, 1060, 275);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (2, 2, 3, 872, 368);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (6, 2, 6, 205, 372);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (8, 2, 7, 271, 261);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (9, 2, 8, 903, 237);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (11, 2, 9, 434, 350);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (12, 2, 10, 671, 345);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (13, 2, 11, 506, 160);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (15, 2, 13, 387, 192);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (16, 2, 14, 67, 278);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (17, 2, 15, 1074, 229);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (18, 2, 16, 75, 340);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (19, 2, 17, 1015, 304);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (1, 1, 25, 75, 165);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (22, 1, 13, 412, 165);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (20, 1, 20, 105, 260);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (24, 1, 14, 376, 260);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (26, 1, 22, 88, 375);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (27, 1, 6, 412, 377);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (25, 1, 4, 244, 150);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (21, 1, 16, 244, 320);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (31, 2, 1, 160, 210);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (14, 2, 12, 673, 157);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (49, 4, 15, 82, 219);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (50, 4, 19, 396, 219);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (51, 4, 5, 236, 233);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (52, 4, 13, 66, 145);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (55, 4, 25, 414, 145);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (56, 4, 17, 236, 314);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (53, 4, 3, 62, 373);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (54, 4, 21, 408, 373);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (4, 3, 25, 730, 187);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (5, 4, 2, 236, 125);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (40, 3, 26, 467, 340);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (41, 3, 22, 920, 372);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (42, 3, 21, 256, 372);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (43, 3, 16, 1057, 332);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (44, 3, 17, 105, 323);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (45, 3, 24, 852, 262);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (46, 3, 23, 199, 244);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (39, 3, 27, 656, 345);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (48, 3, 19, 57, 239);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (38, 3, 28, 443, 155);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (37, 3, 29, 600, 155);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (35, 3, 1, 930, 210);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (57, 5, 1, 199, 232);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (58, 5, 4, 393, 232);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (59, 5, 18, 732, 232);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (61, 5, 2, 912, 232);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (62, 5, 5, 1021, 232);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (63, 5, 13, 405, 421);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (64, 5, 25, 405, 45);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (28, 1, 30, 244, 75);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (3, 4, 30, 240, 45);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (60, 5, 30, 583, 237);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (30, 2, 30, 580, 70);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (36, 3, 30, 540, 70);

INSERT INTO config_elements_for_car_positions (id, id_car_position, id_car_element, "axis_X", "axis_Y")
VALUES (29, 1, 1, 244, 220);

--
-- Data for table public.maintenance (OID = 31991) (LIMIT 0,3)
--
INSERT INTO maintenance (id, id_car, mileage, is_big, date_create)
VALUES (4, 1, 100000, 1, NULL);

INSERT INTO maintenance (id, id_car, mileage, is_big, date_create)
VALUES (1, 1, 100100, 0, NULL);

INSERT INTO maintenance (id, id_car, mileage, is_big, date_create)
VALUES (2, 1, 100200, 0, NULL);

--
-- Data for table public.drivers (OID = 32006) (LIMIT 0,3)
--
INSERT INTO drivers (id, pin, date_last_change)
VALUES (2, 1111, '2017-02-22 15:49:21.169143');

INSERT INTO drivers (id, pin, date_last_change)
VALUES (1, 1111, '2017-02-27 14:51:00');

INSERT INTO drivers (id, pin, date_last_change)
VALUES (0, 7713, '2017-02-27 19:20:51.570273');

--
-- Data for table public.statuses (OID = 41871) (LIMIT 0,4)
--
INSERT INTO statuses (id, name)
VALUES (1, 'Ожид. рем. зона');

INSERT INTO statuses (id, name)
VALUES (2, 'Рем. зона');

INSERT INTO statuses (id, name)
VALUES (3, 'Ожид. в парк');

INSERT INTO statuses (id, name)
VALUES (4, 'В парке');

--
-- Data for table public.info_about_cars (OID = 41890) (LIMIT 0,82)
--
INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (4, 1, NULL, '2017-02-15', '2017-03-15', NULL, NULL, 100500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (5, 4, NULL, '2017-02-15', '2017-03-15', NULL, '2017-01-11 15:33:51', 100500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (6, 4, NULL, '2017-02-15', '2017-03-15', NULL, '2017-01-11 15:41:42', 100500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (7, 4, NULL, '2017-02-15', '2017-03-15', NULL, '2017-01-11 15:42:17', 100500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (8, 4, NULL, '2017-01-27', '2017-01-18', NULL, '2017-01-11 15:57:38', 100505, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (9, 4, NULL, '2017-04-11', '2017-03-15', NULL, '2017-01-11 16:04:46', 100506, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (10, 1, NULL, '2017-02-15', '2017-03-15', 1, '2017-01-11 16:09:57', 100500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (11, 1, NULL, '2017-03-11', '2017-02-11', 1, '2017-01-11 16:14:39', 100500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (12, 1, NULL, '2017-03-11', '2017-02-11', 3, '2017-01-11 16:15:33', 100500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (13, 1, NULL, '2017-03-11', '2017-02-11', 3, '2017-01-11 16:33:39', 100600, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (14, 1, NULL, '2017-03-11', '2017-02-11', 3, '2017-01-11 16:34:38', 100650, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (15, 5188, NULL, '2017-01-11', '2017-01-11', 2, '2017-01-11 16:47:58', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (16, 5188, NULL, '2017-01-11', '2017-01-11', 1, '2017-01-11 16:50:58', 150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (17, 858, NULL, '2017-01-11', '2017-01-11', 1, '2017-01-11 16:53:48', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (18, 2201, NULL, '2017-01-11', '2017-01-11', 1, '2017-01-11 16:55:39', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (19, 5188, NULL, '2017-01-11', '2017-01-11', 1, '2017-01-11 16:58:31', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (20, 10118, NULL, '2017-01-12', '2017-01-12', 2, '2017-01-12 10:45:21', 235000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (21, 10118, NULL, '2017-01-12', '2017-01-12', 1, '2017-01-12 10:49:37', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (22, 10118, NULL, '2017-01-12', '2017-01-12', 2, '2017-01-12 11:12:02', 55555, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (23, 10118, NULL, '2017-01-12', '2017-01-12', 1, '2017-01-12 11:25:09', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (24, 5188, NULL, '2017-01-12', '2017-01-12', 1, '2017-01-12 12:23:17', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (25, 1, NULL, '2017-03-11', '2017-02-11', 2, '2017-01-12 12:48:49', 100670, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (26, 134, NULL, '2017-01-12', '2017-01-12', 2, '2017-01-12 13:37:53', 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (27, 134, NULL, '2017-01-12', '2017-01-12', 1, '2017-01-12 13:42:53', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (28, 1, NULL, '2017-01-07', '2017-01-19', 1, '2017-01-12 14:59:40', 100555, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (29, 1, NULL, '2017-01-07', '2017-01-15', 1, '2017-01-15 20:07:45', 100555, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (30, 1, NULL, '2017-01-07', '2017-01-15', 3, '2017-01-16 14:00:51', 100555, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (31, 1, NULL, '2017-01-17', '2017-01-17', 3, '2017-01-17 14:15:35', 100555, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (32, 1, NULL, '2017-01-17', '2017-01-17', 2, '2017-01-17 14:32:34', 100555, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (33, 1, NULL, '2017-01-17', '2017-01-20', 2, '2017-01-17 14:37:18', 100555, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (34, 1, NULL, '2017-01-17', '2017-01-20', 3, '2017-01-17 15:30:15', 100555, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (35, 1, NULL, '2017-01-17', '2017-01-20', 1, '2017-01-17 16:36:43', 100555, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (36, 1, NULL, '2017-01-17', '2017-01-20', 2, '2017-01-18 11:26:19', 100600, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (37, 1, NULL, '2017-01-17', '2017-01-20', 3, '2017-01-23 11:29:56', 100600, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (38, 1, NULL, '2017-01-17', '2017-01-20', 3, '2017-01-23 15:10:26', 100900, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (39, 1, NULL, '2017-01-17', '2017-01-20', 3, '2017-01-25 15:12:15', 1001111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (40, 1, NULL, '2017-01-27', '2017-01-26', 3, '2017-01-25 15:19:43', 1001111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (41, 134, NULL, '2017-01-30', '2017-01-30', 1, '2017-01-30 15:30:28', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (42, 10261, NULL, '2017-01-30', '2017-01-30', 1, '2017-01-30 17:05:10', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (43, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-02 12:31:21', 1001111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (44, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-07 18:17:11', 1001111, '2017-02-07', 0, '123456', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (45, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-07 18:23:20', 1001111, '2017-02-07', 0, '123456', '', '', 'диаг картатданные', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (46, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-07 18:25:22', 1001111, '2017-02-07', 0, '123456', '', '195 165', 'диаг картатданные', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (47, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-07 18:25:57', 1001111, '2017-02-07', 147, '123456', '', '195 165', 'диаг картатданные', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (48, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-07 18:28:28', 1001111, '2017-02-07', 147, '123456', '', '195 165', 'диаг картатданные', 'нет', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (49, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-07 18:30:45', 1001111, '2017-02-07', 147, '123456', '666666', '195 165', 'диаг картатданные', 'нет', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (50, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-07 18:31:51', 1001111, '2017-02-07', 147, '123456', '666666', '195 165', '', 'нет', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (51, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-07 18:32:27', 1001111, '2017-02-24', 147, '123456', '666666', '195 165', 'да', 'нет', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (52, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-08 08:31:26', 1001111, '2017-02-24', 147, '123456', '666666', '195 165 r15', 'да', 'нет', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (53, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-08 08:38:00', 10, '2017-02-24', 147, '123456', '666666', '195 165 r15', 'да', 'нет', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (54, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-08 08:38:41', 100000, '2017-02-24', 147, '123456', '666666', '195 165 r15', 'да', 'нет nn', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (55, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-08 08:44:53', 110000, '2017-02-24', 147, '123456', '666666', '195 165 r15', 'да', 'нет nn', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (56, 1, NULL, '2017-01-27', '2017-02-02', 3, '2017-02-08 08:48:57', 110000, '2017-02-24', 0, '', '', '', '', 'нет', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (58, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 11:39:06', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (59, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 11:39:07', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (60, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:11', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (61, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:12', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (62, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:14', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (63, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:14', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (64, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:44', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (65, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:44', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (66, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:44', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (67, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:44', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (68, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:44', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (69, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:44', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (70, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:44', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (71, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:44', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (72, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:44', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (73, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:47', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (74, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:06:47', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (75, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:07:07', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (77, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:07:07', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (76, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:07:07', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (78, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:07:17', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (79, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:07:17', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (80, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:07:20', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (81, 10261, NULL, '2017-04-29', '2017-04-29', 1, '2017-04-29 12:07:35', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (82, 10261, 11, '2017-04-29', '2017-04-30', 1, '2017-04-29 12:14:58', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (83, 10261, 11, '2017-04-29', '2017-04-29', 1, '2017-04-29 13:24:39', 0, '2017-04-30', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (84, 10261, 11, '2017-04-29', '2017-04-29', 1, '2017-04-29 14:38:44', 0, '2017-04-29', 0, '', '', '', '', '', NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (1, 1, NULL, '2017-01-05', '2017-01-05', 1, '2017-05-02 18:09:51', 100400, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO info_about_cars (id, id_car, id_park, insurance_end, diagnostic_card_end, id_tire, date_create, mileage, osago_date, osago_number, pts, srts, tire_marka, diagnostic_card_data, child_restraint_means, status)
VALUES (57, 1, NULL, '2017-01-05', '2017-02-12', 3, '2017-05-04 10:01:39', 110000, '2017-02-08', 0, '', '', '', '', '', 1);

--
-- Definition for index rubber_type_pkey (OID = 31842) : 
--
ALTER TABLE ONLY tires_type
    ADD CONSTRAINT rubber_type_pkey
    PRIMARY KEY (id);
--
-- Definition for index car_вamage_pkey (OID = 31864) : 
--
ALTER TABLE ONLY car_damage
    ADD CONSTRAINT "car_вamage_pkey"
    PRIMARY KEY (id);
--
-- Definition for index photos_pkey (OID = 31875) : 
--
ALTER TABLE ONLY photos
    ADD CONSTRAINT photos_pkey
    PRIMARY KEY (id);
--
-- Definition for index car_accessories_pkey (OID = 31886) : 
--
ALTER TABLE ONLY car_accessories
    ADD CONSTRAINT car_accessories_pkey
    PRIMARY KEY (id);
--
-- Definition for index accessories_pkey (OID = 31897) : 
--
ALTER TABLE ONLY accessories
    ADD CONSTRAINT accessories_pkey
    PRIMARY KEY (id);
--
-- Definition for index users_pkey (OID = 31908) : 
--
ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey
    PRIMARY KEY (id);
--
-- Definition for index elements_of_cars_pkey (OID = 31920) : 
--
ALTER TABLE ONLY config_elements_of_cars
    ADD CONSTRAINT elements_of_cars_pkey
    PRIMARY KEY (id);
--
-- Definition for index type_damage_cars_pkey (OID = 31931) : 
--
ALTER TABLE ONLY type_damage_cars
    ADD CONSTRAINT type_damage_cars_pkey
    PRIMARY KEY (id);
--
-- Definition for index check_type_pkey (OID = 31942) : 
--
ALTER TABLE ONLY check_type
    ADD CONSTRAINT check_type_pkey
    PRIMARY KEY (id);
--
-- Definition for index damage_from_elements_pkey (OID = 31950) : 
--
ALTER TABLE ONLY damage_from_elements
    ADD CONSTRAINT damage_from_elements_pkey
    PRIMARY KEY (id);
--
-- Definition for index config_car_positions_pkey (OID = 31964) : 
--
ALTER TABLE ONLY config_car_positions
    ADD CONSTRAINT config_car_positions_pkey
    PRIMARY KEY (id);
--
-- Definition for index config_elements_for_car_positions_pkey (OID = 31985) : 
--
ALTER TABLE ONLY config_elements_for_car_positions
    ADD CONSTRAINT config_elements_for_car_positions_pkey
    PRIMARY KEY (id);
--
-- Definition for index maintenance_pkey (OID = 31995) : 
--
ALTER TABLE ONLY maintenance
    ADD CONSTRAINT maintenance_pkey
    PRIMARY KEY (id);
--
-- Definition for index drivers_pkey (OID = 32009) : 
--
ALTER TABLE ONLY drivers
    ADD CONSTRAINT drivers_pkey
    PRIMARY KEY (id);
--
-- Definition for index statuses_pkey (OID = 41878) : 
--
ALTER TABLE ONLY statuses
    ADD CONSTRAINT statuses_pkey
    PRIMARY KEY (id);
--
-- Definition for index table_pkey (OID = 41897) : 
--
ALTER TABLE ONLY info_about_cars
    ADD CONSTRAINT table_pkey
    PRIMARY KEY (id);
--
-- Data for sequence public.rubber_type_id_seq (OID = 31833)
--
SELECT pg_catalog.setval('rubber_type_id_seq', 3, true);
--
-- Data for sequence public."car_вamage_id_seq" (OID = 31855)
--
SELECT pg_catalog.setval('"car_вamage_id_seq"', 129, true);
--
-- Data for sequence public.photos_id_seq (OID = 31866)
--
SELECT pg_catalog.setval('photos_id_seq', 172, true);
--
-- Data for sequence public.car_accessories_id_seq (OID = 31877)
--
SELECT pg_catalog.setval('car_accessories_id_seq', 60, true);
--
-- Data for sequence public.accessories_id_seq (OID = 31888)
--
SELECT pg_catalog.setval('accessories_id_seq', 61, true);
--
-- Data for sequence public.users_id_seq (OID = 31899)
--
SELECT pg_catalog.setval('users_id_seq', 1, false);
--
-- Data for sequence public.elements_of_cars_id_seq (OID = 31910)
--
SELECT pg_catalog.setval('elements_of_cars_id_seq', 32, true);
--
-- Data for sequence public.type_damage_cars_id_seq (OID = 31922)
--
SELECT pg_catalog.setval('type_damage_cars_id_seq', 13, true);
--
-- Data for sequence public.check_type_id_seq (OID = 31933)
--
SELECT pg_catalog.setval('check_type_id_seq', 5, true);
--
-- Data for sequence public.damage_from_elements_id_seq (OID = 31944)
--
SELECT pg_catalog.setval('damage_from_elements_id_seq', 9, true);
--
-- Data for sequence public.config_car_positions_id_seq (OID = 31955)
--
SELECT pg_catalog.setval('config_car_positions_id_seq', 7, true);
--
-- Data for sequence public.config_elements_for_car_positions_id_seq (OID = 31979)
--
SELECT pg_catalog.setval('config_elements_for_car_positions_id_seq', 64, true);
--
-- Data for sequence public.maintenance_id_seq (OID = 31989)
--
SELECT pg_catalog.setval('maintenance_id_seq', 4, true);
--
-- Data for sequence public.statuses_id_seq (OID = 41869)
--
SELECT pg_catalog.setval('statuses_id_seq', 4, true);
--
-- Data for sequence public.info_about_cars_id_seq (OID = 41888)
--
SELECT pg_catalog.setval('info_about_cars_id_seq', 84, true);
--
-- Comments
--
COMMENT ON SCHEMA public IS 'standard public schema';
COMMENT ON COLUMN public.car_damage.status IS 'true - отремонтировано';
COMMENT ON COLUMN public.car_accessories.status IS '1 - есть
2 - нету
3 - неполный комплект';
COMMENT ON COLUMN public.car_accessories.date_update IS 'дата последнего изменения';
COMMENT ON COLUMN public.car_accessories.comment IS 'коментарий к последнему изменению';
COMMENT ON TABLE public.users IS 'The table for storing users data';
COMMENT ON COLUMN public.maintenance.id_car IS 'id автом';
COMMENT ON COLUMN public.maintenance.mileage IS 'километраж, на котором проходилось ТО';
COMMENT ON COLUMN public.maintenance.date_create IS 'время и дата создания записи';
COMMENT ON COLUMN public.drivers.id IS 'id водителя';
COMMENT ON COLUMN public.drivers.pin IS 'pin водителя';
COMMENT ON COLUMN public.info_about_cars.id_car IS 'id авто';
COMMENT ON COLUMN public.info_about_cars.insurance_end IS 'дата окончания страховки';
COMMENT ON COLUMN public.info_about_cars.diagnostic_card_end IS 'дата окончания диагностической карты';
COMMENT ON COLUMN public.info_about_cars.id_tire IS 'тип резины на авто';
COMMENT ON COLUMN public.info_about_cars.date_create IS 'дата созадния записи';
COMMENT ON COLUMN public.info_about_cars.mileage IS 'текущий пробег';
COMMENT ON COLUMN public.info_about_cars.osago_date IS 'дата ОСАГО';
COMMENT ON COLUMN public.info_about_cars.osago_number IS 'номер ОСАГО';
COMMENT ON COLUMN public.info_about_cars.pts IS 'данные ПТС, серия и номер';
COMMENT ON COLUMN public.info_about_cars.srts IS 'данные СРТС, серия и номер';
COMMENT ON COLUMN public.info_about_cars.tire_marka IS 'марка и размер резины';
COMMENT ON COLUMN public.info_about_cars.diagnostic_card_data IS 'данные диагностической карты';
COMMENT ON COLUMN public.info_about_cars.child_restraint_means IS 'детское удерживающее средство';
